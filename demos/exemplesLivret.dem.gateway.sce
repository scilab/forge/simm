// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("exemplesLivret.dem.gateway.sce");
  subdemolist = ["Exemple 1", "Exemple1.dem.sce" ;
                 "Exemple 2", "Exemple2.dem.sce" ;
                 "Exemple 3", "Exemple3.dem.sce" ;
                 "Exemple 4", "Exemple4.dem.sce" ;
                 "Exemple 5", "Exemple5.dem.sce" ;
                 "Exemple 6", "Exemple6.dem.sce" ;
                 "Exemple 7", "Exemple7.dem.sce" ;
                 "Exemple 8", "Exemple8.dem.sce" ;
                 "Exemple 9", "Exemple9.dem.sce" ;
                 "Exemple 10", "Exemple10.dem.sce" ;
                 "Exemple 11", "Exemple11.dem.sce" ;
                 "Exemple 12", "Exemple12.dem.sce" ;
                 "Exemple 13", "Exemple13.dem.sce" ;
                 "Exemple 14", "Exemple14.dem.sce" ;
                 "Exemple 15", "Exemple15.dem.sce" ;
                 "Exemple 16", "Exemple16.dem.sce" ;
                 "Exemple 17", "Exemple17.dem.sce" ;
                 "Exemple 18", "Exemple18.dem.sce" ;
                 "Exemple 19", "Exemple19.dem.sce" ;
                ]; // add demos here
  subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
