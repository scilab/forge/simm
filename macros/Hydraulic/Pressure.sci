//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=Pressure(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs;
        model=arg1.model;
        while %t do
            [ok,isPascal,exprs] = ...
            getvalue([__('Hydraulic - Pressure');__('Convert a signal in pressure (Pa) or (bar)')],..
            [__('Unit of pressure : (0) Bar, (1) Pascal')],..
            list('vec',1),exprs);

            if ~ok then break, end

            //test error syntax
            if isPascal<>1 & isPascal<>0 then
                message(__('Choose 1 or 0 for pressure unit'));
                ok = %f
            end

            if ok then
                model.equations.parameters(2)=list(isPascal)
                graphics.exprs=exprs;
                x.graphics=graphics;x.model=model;
                break
            end
        end


    case 'define' then
        isPascal=0
        model=scicos_model();
        model.sim='SIMM';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        mo=modelica();
        mo.model='Hydraulic.Components.Sources.Pressure';
        mo.inputs=['signal'];
        mo.outputs=['port_B'];
        mo.parameters=list(['isPascal'],...
        list(isPascal),...
        [0]);
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);
        exprs=[sci2exp(isPascal)];
        x=standard_define([2 2],model,exprs,list([],0));
        x.graphics.in_implicit=['I'];
        x.graphics.in_style=[RealInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[HydraulicOutputStyle()];
    end
endfunction
