//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=Turbulent(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        exprs=graphics.exprs;
        model=x.model;

        while %t do
            [ok,DPnom,Qnom,Pref,exprs]=...
              getvalue([__('Hydraulic - Turbulent');__('Resistance with turbulent flow')],...
                       [__('Nominal difference of pressure (Pa)');...
                        __('Nominal flow rate (m^3/s)');...
                        __('pressure ref for sign estimation (Pa)')],...
                       list('vec',1,'vec',1,'vec',1),exprs);
            
            if ~ok then
                break
            end

            if ok then
                model.equations.parameters(2)=list(DPnom,Qnom,Pref)
                graphics.exprs=exprs;
                x.graphics=graphics;x.model=model;
                break
            end
        end

    case 'define' then
      model=scicos_model();
      //parametres par defaut
      DPnom=7000000;
      Qnom=1;
      Pref=0.000000001;
      //modele modelica
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='Hydraulic.Restrictions.Turbulent';
      mo.inputs=['portA'];
      mo.outputs=['portB'];
      mo.parameters=list(['DPnom','Qnom','Pref'],...
                         list(DPnom,Qnom,Pref),...
                         [0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([DPnom;Qnom;Pref]);
      gr_i=[];
      x=standard_define([2 2],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I'];
      x.graphics.in_style=[HydraulicInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[HydraulicOutputStyle()];
    end
endfunction
