//
// 22/06/2016 - D. Violeau
// Bloc permettant de faire des opérations binaires entre signaux

function [x,y,typ]=SIMM_LOGICAL_OP(job,arg1,arg2)
    x=[];
    y=[];
    typ=[];
    select job
    case "set" then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        while %t do
            [ok,rule,exprs]=scicos_getvalue(__("Set parameters"),..
            ["Operator: AND (0), OR (1), NAND (2), NOR (3), XOR (4), XAND (5), NOT (6)"],..
            list("vec",1),exprs)
            if ~ok then
                break,
            end

            rule=int(rule);

            if (rule<0)|(rule>6) then
                message(__("Incorrect operator "+string(rule)+" ; must be 0 to 6."));
                ok=%f
            end


            if ok then
                if (rule==6) then nin=1
                else nin=2
                end
                it=ones(nin,1);
                ot=1;
                in=[ones(nin,1) ones(nin,1)]
                out=[1 1]
                [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),[],[])
                model.sim='SIMM';
                if nin==1 then
                    graphics.in_implicit=['I'];
                    graphics.in_style=[RealInputStyle()];
                    graphics.out_implicit=['I'];
                    graphics.out_style=[RealOutputStyle()];

                    mo=modelica();
                    mo.model='SIMM.NOT';
                    mo.inputs=['u'];
                    mo.outputs=['y'];
                    model.equations=mo;
                else
                    graphics.in_implicit=['I';'I'];
                    graphics.in_style=[RealInputStyle();RealInputStyle()];
                    graphics.out_implicit=['I'];
                    graphics.out_style=[RealOutputStyle()]; 

                    mo=modelica();
                    disp(rule)
                    if rule==0 then 
                        mo.model='SIMM.AND';
                    elseif rule==1 then
                        mo.model='SIMM.OR';
                    elseif rule==2 then
                        mo.model='SIMM.NAND';
                    elseif rule==3 then
                        mo.model='SIMM.NOR';
                    elseif rule==4 then
                        mo.model='SIMM.XOR';
                     elseif rule==5 then
                        mo.model='SIMM.XAND';
                    end
                    mo.inputs=['u1';'u2'];
                    mo.outputs=['y'];
                    model.equations=mo;                       
                end 
            end
            if ok then


                if rule == 0 then
                    label = "AND";
                elseif rule == 1 then
                    label = "OR";
                elseif rule == 2 then
                    label = "NAND";
                elseif rule == 3 then
                    label = "NOR";
                elseif rule == 4 then
                    label = "XOR";
                elseif rule == 5 then
                    label = "XAND";
                elseif rule == 6 then
                    label = "NOT";
                end
                graphics.exprs=exprs;
                graphics.style = ["blockWithLabel;displayedLabel="+label];
                x.graphics=graphics;
                x.model=model
                break
            end
        end
    case "define" then

        rule=0
        model=scicos_model();
        model.sim='SIMM';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        mo=modelica();
        mo.model='SIMM.AND';
        mo.inputs=['u1','u2'];
        mo.outputs=['y'];
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);

        exprs=[string([rule])]
        gr_i=[]
        x=standard_define([2 2],model,exprs,gr_i)
        x.graphics.in_implicit=['I';'I'];
        x.graphics.in_style=[RealInputStyle();RealInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[RealOutputStyle()];
        if rule == 0 then
            label = "AND";
        elseif rule == 1 then
            label = "OR";
        elseif rule == 2 then
            label = "NAND";
        elseif rule == 3 then
            label = "NOR";
        elseif rule == 4 then
            label = "XOR";
        elseif rule == 5 then
            label = "XAND";
        elseif rule == 6 then
            label = "NOT";
        end

        x.graphics.style = ["blockWithLabel;displayedLabel="+label];
    end
endfunction
