// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME+"_L_", macros_path);
endfunction

function buildblocks()
  macros_path = get_absolute_file_path("buildmacros.sce");

  blocks = [
                "SIMM_LOGICAL_OP"
            "SIMM_RELATIONALOP"
                 ]
           
  tbx_build_blocks(toolbox_dir, blocks, macros_path);
endfunction


buildmacros();
getd(get_absolute_file_path("buildmacros.sce"));
buildblocks();
clear buildmacros; // remove buildmacros on stack
