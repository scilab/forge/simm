// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=Stepper(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,R,L,k,n,J,exprs]=...
              getvalue(['StepperMotor';__('Moteur pas à pas')],...
                       [__('R [Ohm]: Résistance statorique');...
                        __('L [H] : Inductance statorique');...
                        __('k [Nm/A]]: fcem');...
                        __('n : nombre de pas par tour');...
                        __('J [kg.m^2]: moment d''inertier du rotor')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          model.equations.parameters(2)=list(R,L,k,n,J)
          
          
          
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          in_label=["va";"vb"]
          x.graphics.in_label=in_label;
          break
      end
     case 'define' then
      model=scicos_model();
      R=1;
      L=0.001;
      k=0.02;
      n=200;
      J=0.00001;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.Stepper';
      mo.inputs=['pa','pb'];
      mo.outputs=['flange_b'];
      mo.parameters=list(['R','L','k','n','J'],...
                         list(R,L,k,n,J),...
                         [0,0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([R;L;k;n;J]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[ElecInputStyle(),ElecInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[RotOutputStyle()];
      in_label=["va";"vb"]
      x.graphics.in_label=in_label;
    end
endfunction
