//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=CEAS_PredefVoltage(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        //      standard_draw(arg1,%f,_CMTS_PositionSensor_dp);
    case 'getinputs' then
        //      [x,y,typ]=_CMTS_PositionSensor_ip(arg1);
    case 'getoutputs' then
        //      [x,y,typ]=_CMTS_PositionSensor_op(arg1);
    case 'getorigin' then
        //      [x,y]=standard_origin(arg1);
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        //exprs=graphics.exprs;
        model=x.model;
        while %t do

            [ok,value,exprs] = getvalue([__('Predefined Source Voltage')],..
            [__('Type of signal : (0) constant, (1) step, (2) ramp, (3) sine, (4) pulse, (5) sawtooth, (6) trapezoid')],..
            list('vec',1),graphics.exprs(1));
            if ~ok then
                break
            end
            if value <0 | value >7 then
                ok = %f
                message(__("Choose type of signal between 0 and 6"))
            end
            if ok then
                select value
                case 0 //constant
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        exprs2=[sci2exp(V)];
                    end
                    while %t do
                        [ok2,V,exprs2]=...
                        getvalue(['';'MEAS_ConstantVoltage';'';__('Source for constant voltage');''],...
                        [__('V [V] : Value of constant voltage')],...
                        list('vec',1),exprs2);
                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.ConstantVoltage';
                        mo.parameters=list(['V'],...
                        list(V),...
                        [0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;

                        //model.equations.parameters(2)=list(V)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];                        
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Const.<br><br><br>"]
                        break
                    end

                case 1 //step
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(offset);sci2exp(startTime)]
                    end
                    while %t do
                        [ok2,V,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_StepVoltage';'';__('Step voltage source');''],...
                        [__('V [V] : Height of step');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.StepVoltage';
                        mo.parameters=list(['V','offset','startTime'],...
                        list(V,offset,startTime),...
                        [0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Step<br><br>"]
                        break
                    end
                case 2 //ramp
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        duration=2;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(duration);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,V,duration,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_RampVoltage';'';__('Ramp voltage source');''],...
                        [__('V [V] : Height of ramp');__('duration [s] : Duration of ramp');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1,'vec',1),exprs2);


                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.RampVoltage';
                        mo.parameters=list(['V','duration','offset','startTime'],...
                        list(V,duration,offset,startTime),...
                        [0,0,0,0]);                        
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,offset,startTime)
                        model.equations.parameters(2)=list(V,duration,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Ramp<br><br>"]
                        break
                    end
                case 3 //sine
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        phase=0;
                        freqHz=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(phase);sci2exp(freqHz);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,V,phase,freqHz,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_SineVoltage';'';__('Sine voltage source');''],...
                        [__('V [V] : Amplitude of sine wave');__('phase [rad] : Phase of sine wave');__('freqHz [Hz] : Frequency of sine wave');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.SineVoltage';
                        mo.parameters=list(['V','phase','freqHz','offset','startTime'],...
                        list(V,phase,freqHz,offset,startTime),...
                        [0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,phase,freqHz,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Sin.<br><br>"]
                        break
                    end
                case 4 //pulse
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        width=50;
                        period=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(width);sci2exp(period);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,V,width,period,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_PulseVoltage';'';__('Pulse voltage source');''],...
                        [__('V [V] : Amplitude of pulse');__('width [-] : Width of pulse in % of period');__('period [s] : Time for one period');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.PulseVoltage';
                        mo.parameters=list(['V','width','period','offset','startTime'],...
                        list(V,width,period,offset,startTime),...
                        [0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,width,period,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Pulse<br><br>"]
                        break
                    end
                case 5 //sawtooth
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        period=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(period);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,V,period,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_SawToothVoltage';'';__('Saw tooth voltage source');''],...
                        [__('V [V] : Amplitude of saw tooth');__('period [s] : Time for one period');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.SawToothVoltage';
                        mo.parameters=list(['V','period','offset','startTime'],...
                        list(V,period,offset,startTime),...
                        [0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,period,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Saw.<br><br>"]
                        break
                    end
                case 6 //trapezoid
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        V=1;
                        rising=0;
                        width=0.5;
                        falling=0;
                        period=1;
                        nperiod=-1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(V);sci2exp(rising);sci2exp(width);sci2exp(falling);sci2exp(period);sci2exp(nperiod);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,V,rising,width,falling,period,nperiod,offset,startTime,exprs2]=...
                        getvalue(['';'CEAS_TrapezoidVoltage';'';__('Trapezoidal voltage source');''],...
                        [__('V [V] : Amplitude of trapezoid');__('rising [s] : Rising duration of trapezoid (>=0)');__('width [s] : Width duration of trapezoid (>=0)');__('falling [s] : Falling duration of trapezoid (>=0)');__('period [s] : Time for one period (>0)');__('nperiod [-] : Number of periods (< 0 means infinite number of periods)');__('offset [V] : Voltage offset');__('startTime [s] : Time offset')],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Coselica.Electrical.Analog.Sources.TrapezoidVoltage';
                        mo.parameters=list(['V','rising','width','falling','period','nperiod','offset','startTime'],...
                        list(V,rising,width,falling,period,nperiod,offset,startTime),...
                        [0,0,0,0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(V,rising,width,falling,period,nperiod,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Trapez.<br><br>"]
                        break
                    end
                end
                break
            end
        end

    case 'define' then
        model=scicos_model();
        model.sim='Coselica';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        model.in=[1];
        model.out=[1];
        V=1;
        mo=modelica();
        mo.model='Modelica.Electrical.Analog.Sources.ConstantVoltage';
        mo.inputs=['p'];
        mo.outputs=['n'];
        mo.parameters=list(['V'],...
        list(V),...
        [0]);
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);
        exprs=['0',sci2exp(V)];
        x=standard_define([2, 2],model,exprs,list([], 0));
        x.graphics.in_implicit=['I'];
        x.graphics.in_style=[ElecInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[ElecOutputStyle()];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Const.<br><br><br>"]
    end
endfunction
