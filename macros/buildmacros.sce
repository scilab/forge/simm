// This file is released under the 3-clause BSD license. See COPYING-BSD.
function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME+"_S_", macros_path);
  lib(macros_path);
  blocks = [
            "ISCOPE"
            "IREP_TEMP"
            "IPARAM_VAR"
            "CEAS_PredefCurrent"
            "CEAS_PredefVoltage"
            "SIMM_POSITIV"
            "RotationalFriction"
            "ExtractPosBar"
            "AnimBar2D"
            "DiodePV"
            "PanneauPV"
            "SIMMUserModel"
            "ServoMotor"
            "PermanentMagnetSyncMotor"
            "WoundedRotorSyncMotor"
            //"SIMM_LOGICAL_OP"
            //"SIMM_RELATIONALOP"
            "SIMM_CONTROL"
            "TrainEpiType1"
            "TrainEpiType2"
            "TrainEpiType3"
            "TrainEpiType4"
            "StepperMotor"
            "SIMM_DELAY"
            "SIMM_EMF_SINUS"
            "SteppingSequence"
            "IdealCurSourcesStepper"
            "IdealInverter"
            "PowerTransistor"
            "OnduleurBLDC"
            "HallLogicBLDC"
            ]

  tbx_build_blocks(toolbox_dir, blocks, macros_path);
endfunction

// Load Utils
getd(get_absolute_file_path("buildmacros.sce")+filesep()+"Utils");

buildmacros();
clear buildmacros; // remove buildmacros on stack

if %t then
    pathB = get_absolute_file_path('buildmacros.sce');
    chdir(pathB);
// directories
    dirs=['Utils','Logic','Hydraulic','Aeraulic'];

    for d = dirs
        if isdir( d ) then
            chdir( d );
            exec('buildmacros.sce');
            chdir( '..' );
        end
    end
    chdir('..');

    clear d dirs pathB
end
