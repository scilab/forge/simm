//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=travee(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;
      while %t do
          [ok,m,J,L,N,exprs]=...
              getvalue(['travee';__('Travée pesante soumise à une ou plusieurs charges ponctuelle')],...
                       [__('m [kg]: masse de la travée');...
                        __('J [kg.m2] : moment d''inertie');...
                        __('L [m]]: longueur de la travée');...
                        __('N : nombre de charges ponctuelles')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1),exprs);
        if ~ok then break,end

            if ok then
                
                names="riveG;"
                for i =1:N
                    names=names+"F"+string(i)+";"
                end
                names=names+"riveD"

                in=strsplit(names,";")
                out=[]
                intypex=[1:size(in,"*")]
                outtypex=[1:size(out,"*")]
                [model,graphics,ok]=set_io(model,graphics,list([ones(in),ones(in)],ones(in)),..
                list([ones(out),ones(out)],ones(out)),..
                [],[],intypex,outtypex)
                for i =[1:N+2]
                    graphics.in_implicit(i)="I"
                    graphics.in_style(i)=TransOutputStyle()
                end
                graphics.in_style(1)=TransInputStyle()
                graphics.in_style(N+2)=TransInputStyle()

                if N==1 then
                    model.equations.model="SIMM.travee1"
                elseif N==1 then
                    model.equations.model="SIMM.travee2"
                end
                model.equations.inputs=in
                model.equations.outputs=[]
                model.equations.parameters(2)=list(m,J,L)
                model.in=ones(size(model.equations.inputs,'*'),1);
                model.out=[];
                graphics.exprs=exprs;
                graphics.in_label=in;
                x.graphics=graphics;x.model=model;                
                graphics.exprs = exprs;

                break
           end

        end

    case 'define' then
      model=scicos_model();
      m=6.6;
      L=1.2;
      J=0.01;
      N=1;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.travee';
      mo.inputs=['riveG','F1','riveD'];
      mo.outputs=[];
      mo.parameters=list(['m','J','L','N'],...
                         list(m,J,L,N),...
                         [0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=[];
      exprs=string([m;J;L;N]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I','I'];
      x.graphics.in_style=[TransInputStyle(),TransOutputStyle(),TransInputStyle()];
      x.graphics.out_implicit=[];
      x.graphics.out_style=[];
          in_label=["riveG";"F1";"riveD"]
      x.graphics.in_label=in_label;
    end
endfunction

