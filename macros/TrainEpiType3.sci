// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=TrainEpiType3(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,Zc1,Zs1,Zc2,Zs2,exprs]=...
              getvalue(['TrainEpiType4';__('Train épicycloïdal de type IV avec porte-satellite et couronnes en entrée et en sortie')],...
                       [__('Zc1 : Nombre de dents de la couronne d''entrée');...
                        __('Zs1 : Nombre de dents du satellite d''entrée');...
                        __('Zc2 : Nombre de dents de la couronne de sortie');...
                        __('Zs2 : Nombre de dents du satellite de sortie')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          model.equations.parameters(2)=list(Zc1,Zs1,Zc2,Zs2)
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      Zc1=40
      Zc2=40
      Zs1=10
      Zs2=10
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.TrainEpiType3';
      mo.inputs=['flange_ps','flange_c1'];
      mo.outputs=['flange_c2'];
      mo.parameters=list(['Zc1','Zs1','Zc2','Zs2'],...
                         list(Zc1,Zs1,Zc2,Zs2),...
                         [0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([Zc1;Zs1;Zc2;Zs2]);
      gr_i=[];
      x=standard_define([3 3],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[RotInputStyle(),RotInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[RotOutputStyle()];
      in_label=["ps";"c1"]
      x.graphics.in_label=in_label;
      out_label=["c2"]
      x.graphics.out_label=out_label;
    end
endfunction
