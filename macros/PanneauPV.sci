// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=PanneauPV(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,Iscnpv,Vocnpv,Nspv,Tpv,apv,Kvpv,Kipv,Rppv,Rspv,exprs]=...
              getvalue(['PanneauPV';__('Source de courant Ipvo proportionnel à l''éclairement')],...
                       [__('Iscnpv [A] : Courant nominal de court-circuit');...
                        __('Vocnpv [V] : Tension nominale en circuit ouvert');...
                        __('Nspv : nombre de cellules');...
                        __('Tpv  [K]: température du panneau, ideal=1');...
                        __('apv : coefficient ajustement');...
                        __('Kvpv [V/K]: coefficient de dependance de V avec la temperature');...
                        __('Kipv [A/K]: coefficient de dependance de I avec la temperature');...
                        __('Rppv [Ohm]: résistance parallèle/shunt');...
                        __('Rspv [Ohm]:résistance série')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          model.equations.parameters(2)=list(Iscnpv,Vocnpv,Nspv,Tpv,apv,Kvpv,Kipv,Rppv,Rspv)
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      Iscnpv=0.231;
      Vocnpv=4.8;
      Nspv=6;
      Tpv=298;
      apv=1.3;
      Kvpv=-0.01728;
      Kipv=0.000138;
      Rppv=1000;
      Rspv=1.5;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.PanneauPV';
      mo.inputs=['eclt'];
      mo.outputs=['p','n'];
      mo.parameters=list(['Iscnpv','Vocnpv','Nspv','Tpv','apv','Kvpv','Kipv','Rppv','Rspv'],...
                         list(Iscnpv,Vocnpv,Nspv,Tpv,apv,Kvpv,Kipv,Rppv,Rspv),...
                         [0,0,0,0,0,0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([Iscnpv;Vocnpv;Nspv;Tpv;apv;Kvpv;Kipv;Rppv;Rspv]);
      gr_i=[];
      x=standard_define([2 2],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I'];
      x.graphics.in_style=[RealInputStyle()];
      x.graphics.out_implicit=['I','I'];
      x.graphics.out_style=[ElecInputStyle(),ElecOutputStyle()];
    end
endfunction
