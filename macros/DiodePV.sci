// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=DiodePV(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,Iscn,Vocn,Ns,Tempe,a,Kv,Ki,exprs]=...
              getvalue(['DiodePV';__('diode panneau solaire')],...
                       [__('Iscn [A] : Courant nominal de court-circuit');...
                        __('Vocn [V] : Tension nominale en circuit ouvert');...
                        __('Ns : nombre de cellules');...
                        __('Tempe  [K]: température du panneau');...
                        __('a : coefficient ajustement');...
                        __('Kv [V/K]: coefficient de dependance de V avec la temperature');...
                        __('Ki [A/K]: coefficient de dependance de I avec la temperature')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          model.equations.parameters(2)=list(Iscn,Vocn,Ns,Tempe,a,Kv,Ki)
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      Iscn=0.231;
      Vocn=4.8;
      Ns=6;
      Tempe=298;
      a=1.15;
      Kv=-0.01728;
      Ki=0.000138;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.DiodePV';
      mo.inputs=['p'];
      mo.outputs=['n'];
      mo.parameters=list(['Iscn','Vocn','Ns','Tempe','a','Kv','Ki'],...
                         list(Iscn,Vocn,Ns,Tempe,a,Kv,Ki),...
                         [0,0,0,0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=[sci2exp(Iscn);sci2exp(Vocn);sci2exp(Ns);sci2exp(Tempe);sci2exp(a);sci2exp(Kv);sci2exp(Ki)];
      gr_i=[];
      x=standard_define([2 2],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I'];
      x.graphics.in_style=[ElecInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[ElecOutputStyle()];
    end
endfunction
