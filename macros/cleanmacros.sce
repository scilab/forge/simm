// ====================================================================
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
function cleanmacros()
  libpath = get_absolute_file_path('cleanmacros.sce');
  
  binfiles = ls(libpath+'/*.bin');
  for i = 1:size(binfiles,'*')
    mdelete(binfiles(i));
  end
  mdelete(libpath+'/*~');
  mdelete(libpath+'/names');
  mdelete(libpath+'/lib');
  
  blockfiles = ls(libpath+'../images/h5/*.h5');
  for i = 1:size(blockfiles,'*')
    mdelete(blockfiles(i));
  end
  blockfiles = ls(libpath+'../images/h5/*.sod');
  for i = 1:size(blockfiles,'*')
    mdelete(blockfiles(i));
  end  
  
  
  
endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack
// ====================================================================

pathB = get_absolute_file_path('cleanmacros.sce');
chdir(pathB);
// directories
dirs=['Hydraulic','Aeraulic'];

for d = dirs
  if isdir( d ) then
    chdir( d );
    exec('cleanmacros.sce');
    chdir( '..' );
  end
end
chdir('..');

clear d dirs pathB
