// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=PowerTransistor(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
        case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      x.graphics=graphics;x.model=model;
     case 'define' then

      model=scicos_model();
      dephini=0;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.PowerTransistor';
      mo.inputs=['c','p'];
      mo.outputs=['n'];
      mo.parameters=list([],...
                         list(),...
                         []);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.gui = 'PowerTransistor';
       x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[RealInputStyle(),ElecInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[ElecOutputStyle()];
      in_label=["c";"p"]
      x.graphics.in_label=in_label;
      out_label=["n"]
      x.graphics.out_label=out_label;
    end
endfunction
