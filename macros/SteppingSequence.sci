// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=SteppingSequence(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,amplitude,starttime,mode_sequence,exprs]=...
              getvalue(['SteppingSequence';__('Sequence de commande d''un moteur pas à pas')],...
                       [__('amplitudes des signaux de sortie');...
                        __('starttime [s] : retard au démarrage des signaux');...
                        __('mode : 0 pas entier, 1 pas entier à couple maximal, 2 demi-pas')],...
                       list('vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          disp(mode_sequence)
          if mode_sequence <> 0 & mode_sequence <>1 & mode_sequence <>2 then 
              message(__('Le mode doit être 0 (pas entier), 1 (pas entier couple maximal) ou 2 (demi-pas) uniquement'));
              disp("break")
              break
          end
          
          model.equations.parameters(2)=list(amplitude,starttime,mode_sequence)

          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      amplitude=1;
      mode_sequence=0;
      starttime=0;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.stepping';
      mo.inputs=['Tcommut','sens'];
      mo.outputs=['yA','yB'];
      mo.parameters=list(['amplitude','starttime','mode'],...
                         list(amplitude,starttime,mode_sequence),...
                         [0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([amplitude;starttime;mode_sequence]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[RealInputStyle(),RealInputStyle()];
      x.graphics.out_implicit=['I','I'];
      x.graphics.out_style=[RealOutputStyle(),RealOutputStyle()];
      in_label=["Tcommut",'sens']
      x.graphics.in_label=in_label;
      out_label=["A","B"]
      x.graphics.out_label=out_label;
    end
endfunction
