//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Fournier
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=SIMM_DELAY(job,arg1,arg2)
	x=[];y=[];typ=[];
	select job
	case 'plot' then
		// deprecated
	case 'getinputs' then
		// deprecater
	case 'getoutputs' then
		// deprecated
	case 'getorigin' then
		// deprecated
	case 'set' then
		x = arg1;
		graphics = arg1.graphics;
		exprs = graphics.exprs;
		model = arg1.model;

		while %t do
			[ok, delay_time, exprs] = scicos_getvalue(..
				[msprintf(gettext("Set %s block parameters"), "SIMM_DELAY");" "; gettext("Delay Function");" "], ..
				[gettext("Delay")], ..
				list("vec",1), ..
				exprs);

			if ~ok then
				break;
			end

			if ok then
				model.rpar.objs(3).graphics.exprs(1) = sci2exp(delay_time);
                model.rpar.objs(3).model.rpar(1)=delay_time
				graphics.exprs = exprs;
				x.graphics = graphics;
                x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=Retard "+sci2exp(delay_time)+" s"]
				x.model = model;
				break;
			end
		end

	case 'define' then
		delay_time = 1;
		

		outputport = CBI_RealOutput('define');
		outputport.graphics.pin = 8;
		outputport.graphics.pout = 6;

		inputport = CBI_RealInput('define');
		inputport.graphics.pin = 7;
		inputport.graphics.pout = 9;

		delay = TIME_DELAY('define');
		delay.graphics.pin = 6;
		delay.graphics.pout = 7;
        delay.graphics.exprs(1)=sci2exp(delay_time)
        delay.model.rpar(1)=delay_time
        
        
		inimpl = INIMPL_f('define');
		inimpl.graphics.pout = 6;
		
		outimpl = OUTIMPL_f('define');
		outimpl.graphics.pin = 7;

		diagram = scicos_diagram();
		diagram.objs(1) = outputport;
		diagram.objs(2) = inputport;
		diagram.objs(3) = delay;
		diagram.objs(4) = inimpl;
        diagram.objs(5) = outimpl;
		diagram.objs(6) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[3, 1, 1]);
		diagram.objs(7) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[2, 1, 1]);
		diagram.objs(8) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 2], from=[4, 1, 0], to=[1, 1, 1]); 
		diagram.objs(9) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 2], from=[2, 1, 0], to=[5, 1, 1]);

		model = scicos_model();
		model.sim = 'super';
		model.in = -1;
		model.in2 = -2;
		model.intyp = -1;
		model.out = -1;
		model.out2 = -2;
		model.outtyp = -1;
		model.blocktype = 'h';
		model.dep_ut = [%f, %f];
		model.rpar = diagram;

		x = standard_define([4 2], model, "", []);
		x.gui = 'SIMM_DELAY';
		x.graphics.exprs = [string(delay_time)];
        x.graphics.in_implicit=['I'];
      x.graphics.in_style=[RealInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[RealOutputStyle()];
      x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=Retard 1 s"]
	end
endfunction
