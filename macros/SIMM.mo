

package SIMM

    model POSITIV
      Modelica.Blocks.Interfaces.RealInput u ;
      Modelica.Blocks.Math.Abs abs1 ;
      Modelica.Blocks.Math.Add add1 ;
      Modelica.Blocks.Math.Gain gain1(k = 1 / 2) ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
      connect(u,add1.u2) ;
      connect(add1.y,gain1.u) ;
      connect(gain1.y,y) ;
      connect(abs1.y,add1.u1) ;
      connect(u,abs1.u) ;
    end POSITIV;

    model AND
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if (abs(u1.signal)>0 and abs(u2.signal)>0 ) then y.signal=1;
        else y.signal=0;
        end if;
    end AND;


    model OR
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if (abs(u1.signal)<=0 and abs(u2.signal)<=0 ) then y.signal=0;
        else y.signal=1;
        end if;
    end OR;    

    model NAND
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if (abs(u1.signal)>0 and abs(u2.signal)>0 ) then y.signal=0;
        else y.signal=1;
        end if;
    end NAND;


    model NOR
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if (abs(u1.signal)<=0 and abs(u2.signal)<=0 ) then y.signal=1;
        else y.signal=0;
        end if;
    end NOR;    

    model XOR
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if ((abs(u1.signal)<=0 and abs(u2.signal)>0) or (abs(u1.signal)>0 and abs(u2.signal)<=0)) then y.signal=1;
        else y.signal=0;
        end if;
    end XOR;
    
     model XAND
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if ((abs(u1.signal)<=0 and abs(u2.signal)>0) or (abs(u1.signal)>0 and abs(u2.signal)<=0)) then y.signal=0;
        else y.signal=1;
        end if;
    end XAND;
    
    model NOT
      Modelica.Blocks.Interfaces.RealInput u ;
      Modelica.Blocks.Interfaces.RealOutput y ;

    equation
           if (abs(u.signal)>0) then y.signal=0;
           else y.signal=1;
               end if;
    end NOT;

    model EGAL
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if abs(u1.signal- u2.signal)<=0 then
          y.signal= 1;
        else y.signal=0;
        end if;

    end EGAL;

    model DIFF
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation

         if abs(u1.signal- u2.signal)<=0 then
          y.signal= 0;
        else y.signal=1;
        end if;
    end DIFF;

    model INF
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation
        if (u1.signal < u2.signal ) then y.signal=1;
        else y.signal=0;
        end if;

    end INF;
 
     model INFEGAL
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation

        if (u1.signal <= u2.signal ) then y.signal=1;
        else y.signal=0;
        end if;

    end INFEGAL;

    model SUP
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation

        if (u1.signal > u2.signal ) then y.signal=1;
        else y.signal=0;
        end if;

    end SUP;
 
     model SUPEGAL
      Modelica.Blocks.Interfaces.RealInput u1 ;
      Modelica.Blocks.Interfaces.RealInput u2 ;
      Modelica.Blocks.Interfaces.RealOutput y ;
    equation

        if (u1.signal >= u2.signal ) then y.signal=1;
        else y.signal=0;
        end if;

    end SUPEGAL;
        
    partial model PartialCompliant "Compliant connection of two translational 1D flanges"
      Modelica.Mechanics.Translational.Interfaces.Flange_a flange_a "Left flange of compliant 1-dim. translational component";
      Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b "Right flange of compliant 1-dim. translational component" ;
      Real s_rel(start = 0) "relative distance (= flange_b.s - flange_a.s)";
      Real f "force between flanges (positive in direction of flange axis R)";
    equation
      s_rel = flange_b.s - flange_a.s;
      flange_b.f = f;
      flange_a.f = -f;
    end PartialCompliant;

    package SIunits
        type AbsoluteActivity = Real ;
        type AbsolutePressure = Pressure ;
        type Pressure = Real ;
        type VolumeFlowRate = Real ;
        type Area = Real;
        type Length = Real ;
        type Velocity = Real ;
        type Volume = Real ;
        type Power = Real ;
        type AngularVelocity = Real ;
        type Efficiency = Real ;
        type Mass = Real ;
        type Force = Real ;
        type Torque = Real ;
        type Distance = Real;
           
    end SIunits;

      model RotationalFriction "Rotational Damping and Coulomb friction"
        extends Modelica.Mechanics.Rotational.Interfaces.TwoFlangesAndBearing;
        Real phi "Absolute angular position of flange_a and flange_b";
        Real w "Absolute angular velocity of flange_a and flange_b";
        Real a "Absolute angular acceleration of flange_a and flange_b";
        //Real tau ; 
        parameter Real Tau_prop = 1 "Angular velocity dependent friction";
        parameter Real Tau_Coulomb = 5 "Constant fricton: Coulomb torque";
        //Real epsilon = 0.001;
      equation
        phi = phi_a;
        phi = phi_b;
        w = der(phi);
        a = der(w);
        bearing.phi = 0;
        0 = flange_a.tau + flange_b.tau - Tau_prop * w - Tau_Coulomb*tanh(100*w) ;
        //tau = if w>=epsilon then Tau_Coulomb else -1*Tau_Coulomb ;
      end RotationalFriction;

    model TrainEpiType1
        //parameters
        parameter Real Zc = 4.000000e+01;
        parameter Real Zp = 1.000000e+01;
        //input variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_ps;
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_p;
        //output variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_c;
        Real k=-Zp/Zc;
    equation
        flange_c.phi-k*flange_p.phi+(k-1)*flange_ps.phi=0;
        flange_p.tau=k*flange_c.tau;
        flange_ps.tau=flange_c.tau*(1+k);

    end TrainEpiType1;

    model TrainEpiType2
        //parameters
        parameter Real Zc = 4.000000e+01;
        parameter Real Zp = 1.000000e+01;
        parameter Real Zs1 = 10;
        parameter Real Zs2 = 10;
        //input variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_ps;
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_p;
        //output variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_c;
        Real k=-Zp*Zs2/(Zs1*Zc);
    equation
        flange_c.phi-k*flange_p.phi+(k-1)*flange_ps.phi=0;
        flange_p.tau=k*flange_c.tau;
        flange_ps.tau=flange_c.tau*(1+k);
    end TrainEpiType2;

    model TrainEpiType3
        //parameters
        parameter Real Zc1 = 4.000000e+01;
        parameter Real Zc2 = 4.000000e+01;
        parameter Real Zs1 = 10;
        parameter Real Zs2 = 10;
        //input variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_ps;
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_c1;
        //output variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_c2;
        Real k=-Zc1*Zs2/(Zs1*Zc2);
    equation
        flange_c2.phi-k*flange_c1.phi+(k-1)*flange_ps.phi=0;
        flange_c1.tau=k*flange_c2.tau;
        flange_ps.tau=flange_c2.tau*(1+k);

    end TrainEpiType3;

    model TrainEpiType4
        //parameters
        parameter Real Zp1 = 1.000000e+01;
        parameter Real Zp2 = 1.000000e+01;
        parameter Real Zs1 = 10;
        parameter Real Zs2 = 10;
        //input variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_ps;
        Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_p1;
        //output variables
        Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_p2;
        Real k=-Zp1*Zs2/(Zs1*Zp2);
    equation
        flange_p2.phi-k*flange_p1.phi+(k-1)*flange_ps.phi=0;
        flange_p1.tau=k*flange_p2.tau;
        flange_ps.tau=flange_p2.tau*(1+k);

    end TrainEpiType4;

model PowerTransistor
  ////Generation automatique ////
      //input variables
      Modelica.Blocks.Interfaces.RealInput c;
      //les variables disponibles sont u.signal signal sans unité, 
      Modelica.Electrical.Analog.Interfaces.PositivePin p;
      //les variables disponibles sont p.v potentiel en V, p.i courant en A, 
      //output variables
      Modelica.Electrical.Analog.Interfaces.NegativePin n;
      //les variables disponibles sont n.v potentiel en V, n.i courant en A, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;

      Coselica.Electrical.Analog.Ideal.IdealDiode diode;
      Modelica.Electrical.Analog.Ideal.IdealClosingSwitch switch;
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
connect(diode.n,switch.n);
connect(diode.p,switch.p);
connect(switch.control,c);
connect(switch.p,p);
connect(switch.n,n);
end PowerTransistor;

model OnduleurBLDC
  ////Generation automatique ////
      //input variables
      Modelica.Blocks.Interfaces.RealInput c[6];
      //les variables disponibles sont c.signal signal sans unité, 
      Modelica.Electrical.Analog.Interfaces.PositivePin p;
      //les variables disponibles sont p.v potentiel en V, p.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin n;
      //les variables disponibles sont n.v potentiel en V, n.i courant en A, 
      //output variables
      Modelica.Electrical.Analog.Interfaces.NegativePin pa;
      //les variables disponibles sont pa.v potentiel en V, pa.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin pb;
      //les variables disponibles sont pb.v potentiel en V, pb.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin pc;
      //les variables disponibles sont pc.v potentiel en V, pc.i courant en A, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
SIMM.PowerTransistor T1;
SIMM.PowerTransistor T2;
SIMM.PowerTransistor T3;
SIMM.PowerTransistor T4;
SIMM.PowerTransistor T5;
SIMM.PowerTransistor T6;
Coselica.Blocks.Routing.DeMultiplex2 mult1;
Coselica.Blocks.Routing.DeMultiplex2 mult2;
Coselica.Blocks.Routing.DeMultiplex2 mult3;
Coselica.Blocks.Routing.DeMultiplexVector2 multV1(n1=2,n2=2);
Coselica.Blocks.Routing.DeMultiplexVector2 multV2(n1=4,n2=2);
Modelica.Electrical.Analog.Basic.Ground gnd;
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
connect(T1.n,p);
connect(T1.n,T2.n);
  connect(T2.n,T3.n); 

  connect(T4.n,T1.p); 
  connect(T5.n,T2.p); 
  connect(T6.n,T3.p); 
  
  connect(T4.n,pa); 
  connect(T5.n,pb); 
  connect(T6.n,pc);
  
  connect(T4.p,gnd.p); 
  connect(T5.p,gnd.p); 
  connect(T6.p,gnd.p); 
  
  connect(T4.p,n);
  
  connect(mult1.y1,T1.c);
  connect(mult1.y2,T2.c);
  connect(mult2.y1,T3.c);
  connect(mult2.y2,T4.c);
  connect(mult3.y1,T5.c);
  connect(mult3.y2,T6.c);
  
  connect(mult1.u,multV1.y1);
  connect(mult2.u,multV1.y2);
  connect(multV1.u,multV2.y1);
  connect(multV2.u,c);    
  connect(multV2.y2,mult3.u);    
  
  
  
end OnduleurBLDC;

model HallLogicBLDC
  ////Generation automatique ////
      //parameters
      parameter Real dephIni = 1.570800e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput thetae;
      //les variables disponibles sont thetae.signal signal sans unité, 
      //output variables
      Modelica.Blocks.Interfaces.RealOutput s[6];
      Modelica.Blocks.Interfaces.RealOutput s1;
      //les variables disponibles sont s1.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput s2;
      //les variables disponibles sont s2.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput s3;
      //les variables disponibles sont s3.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput s4;
      //les variables disponibles sont s4.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput s5;
      //les variables disponibles sont s5.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput s6;
      //les variables disponibles sont s6.signal signal sans unité, 
  ////Ne pas modifier avant cette ligne ////
Coselica.Blocks.Routing.Multiplex2 mult1;
Coselica.Blocks.Routing.Multiplex2 mult2;
Coselica.Blocks.Routing.Multiplex2 mult3;
Coselica.Blocks.Routing.MultiplexVector2 multV1(n1=2,n2=2);
Coselica.Blocks.Routing.MultiplexVector2 multV2(n1=4,n2=2);
 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Real C1,C2,C3;
Real e1,e2,e3,e4,e5,e6;
Real e;
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
  
  connect(mult1.y,multV1.u1);
  connect(mult2.y,multV1.u2);
  connect(mult3.y,multV2.u2);
  connect(multV1.y,multV2.u1);    
  
  mult1.u1=s1;
  mult1.u2=s2;
  mult2.u1=s3;
  mult2.u2=s4;
  mult3.u1=s5;
  mult3.u2=s6;  
  connect(multV2.y,s);  
  
//etat des capteurs
e=thetae.signal+dephIni;
C1=if sin(e)>=0 then 1 else 0;
C2=if sin(e-2*3.1416/3)>=0 then 1 else 0;
C3=if sin(e+2*3.1416/3)>=0 then 1 else 0;
//6 etats possibles
e1=if (C1 < 0.5 and C2 >0.5 and C3<0.5) then 1 else 0;
e2=if (C1 < 0.5 and C2 >0.5 and C3>0.5) then 1 else 0;
e3=if (C1 < 0.5 and C2 <0.5 and C3>0.5) then 1 else 0;
e4=if (C1 > 0.5 and C2 <0.5 and C3>0.5) then 1 else 0;
e5=if (C1 > 0.5 and C2 <0.5 and C3<0.5) then 1 else 0;
e6=if (C1 > 0.5 and C2 >0.5 and C3<0.5) then 1 else 0;
//interrupteurs a fermer
s1.signal=if e1>0.5 or e6>0.5 then 1 else 0;
s2.signal=if e2>0.5 or e3>0.5 then 1 else 0;
s3.signal=if e4>0.5 or e5>0.5 then 1 else 0;
s4.signal=if e3>0.5 or e4>0.5 then 1 else 0;
s5.signal=if e5>0.5 or e6>0.5 then 1 else 0;
s6.signal=if e1>0.5 or e2>0.5 then 1 else 0;


end HallLogicBLDC;

// modifications T. Royant 11 2014        actu 5 / 5 /2015
         model DiodePV "diode PV"
          extends Modelica.Electrical.Analog.Interfaces.OnePort;
          parameter Real Iscn = 0.23 "courant court-circuit nominal";
          parameter Real Vocn = 4.80 "tension nominale en circuit ouvert";
          parameter Real Ns = 6 "nombre de cellules";
          parameter Real Tempe = 298 "temperature du panneau en K";
          parameter Real a = 1.15 "coefficient d ajustement";
          parameter Real Kv = -0.01728 "coefficient de dependance de la tension avec la temperature V/K";
          parameter Real Ki = 0.000138 "coefficient de dependance du courant avec la temperature A/K";
         protected
          Real A;
          Real B;
           function pow "Just a helper function for x^y"
            input Real x;
            input Real y;
            Real z;
          algorithm
            z:=x ^ y;
          end pow;
        equation
          //i = (Iscn+Ki*(T-298))*(exp(1.6e-19*v/(a*1.38e-23*T*Ns))-1)/exp(1.6e-19*(Vocn+Kv*(T-298))/(a*1.38e-23*T*Ns)-1);
          B=exp(1.6e-19/(a*1.38e-23*Tempe*Ns));
          A=exp(1.6e-19*(Vocn+Kv*(Tempe-298))/(a*1.38e-23*Tempe*Ns))-1; // modif 5 2015
          i= (Iscn+Ki*(Tempe-298))*(B^v-1)/A;
        end DiodePV;

    model PanneauPV
         // Thierry Royant - Lycée Le Dantec Lannion - 1.1  (24/5/2015)
         //  en entrée signal : l'éclairement en W/m2
         // en sortie : 2 bornes p et n électriques
         // paramètrage avec les valeurs caractéristiques du panneau
        // !!!! NB : il faut impérativement insérer la définition de la diodePV dans le fichier coselica.mo utilisé  !!!!
        
         // Terminaux :
         Pin p,n;  // déclaration des broches p et n connecteurs
         
          //parameters
          parameter Real Iscnpv ;
          parameter Real Vocnpv ;
          parameter Real Nspv ;
          parameter Real Tpv;
          parameter Real apv ;
          parameter Real Kvpv ;
          parameter Real Kipv ;
          parameter Real Rppv;
          parameter Real Rspv;
         
              //entrée signal Eclairement
        Modelica.Blocks.Interfaces.RealInput eclt; // port d'entée signal

        // éléments internes
        //Real Rpp=1e7;
        Modelica.Electrical.Analog.Basic.Resistor RP(R = Rppv) ;
        Modelica.Electrical.Analog.Basic.Resistor RS(R = Rspv) ;
        DiodePV DiodePV1(Iscn=Iscnpv , Vocn=Vocnpv , Ns=Nspv , Tempe=Tpv , a=apv ,Kv=Kvpv , Ki=Kipv);
        Pin mid; // noeud interne
        
        equation
        //connexions
         connect(RP.p,mid);
         connect(RP.n,n);
         connect(DiodePV1.p,mid);
         connect(DiodePV1.n,n);
         connect(RS.p,p);
         connect(RS.n,mid);

        p.i=-(eclt.signal*Iscnpv/1000+Kipv*Tpv)+RP.p.i+DiodePV1.p.i; // loi des noeuds au point "mid" ; le courant issu de la broche Ipvo dépend de l'éclairement
        // p.i est le courant qui rentre par la borne p (+) du panneau
    end PanneauPV;
    // fin modifs TR

    model ServoMotor
      parameter Real U = 6.000000e+00;
      parameter Real Cr = 4.400000e-01;
      parameter Real T = 1.500000e-01;
      parameter Real angle = 6.000000e+01;

      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Modelica.Blocks.Interfaces.RealInput u;
      parameter Real wr=angle*3.1416/180/T;
      parameter Real ksr=U/wr;
      parameter Real R1 =ksr*U/Cr;
      //parameter Real R1 =U*U/(Cr*wr);
      parameter Real L1=0.001;
      parameter Real k1=ksr*0.02;  
      parameter Real ratio1=0.02;
      parameter Real invRatio1=50;
      parameter Real tau=T/20;
      parameter Real Jrotor1=(k1)^2*tau/R1;
      parameter Real k1_61 =1 ;
      parameter Real k2_62 =-1  ;
      parameter Real Kp =ksr/(4*tau);
      parameter Real uMax1 = U;
      parameter Real uMin1 =-U;
      Modelica.Electrical.Machines.Components.DCmotor0 	DCmotor01(R=R1, L=L1, k=k1, Jrotor=Jrotor1);
      Modelica.Mechanics.Rotational.IdealGear0 	IdealGear01(ratio=invRatio1);
      Modelica.Electrical.Analog.Sources.SignalVoltage 	SignalVoltage1;
      Modelica.Electrical.Analog.Basic.Ground 	Ground1;
      MMRS_AngleSensor AngleSensor1;
      Modelica.Blocks.Math.Add 	Add1(k1=k1_61, k2=k2_62);
      Modelica.Blocks.Math.Gain 	Gain1(k=Kp);
      Modelica.Blocks.Math.Gain 	Gain2(k=3.1416/180);
      Modelica.Blocks.Nonlinear.Limiter 	Limiter1(uMax=uMax1, uMin=uMin1);
    equation

   connect (AngleSensor1.phi,Add1.u2);
  connect (SignalVoltage1.n,DCmotor01.n);
  connect (Ground1.p,DCmotor01.n);
  connect (DCmotor01.p,SignalVoltage1.p);
  connect (IdealGear01.flange_a,DCmotor01.rotor);
  connect (AngleSensor1.flange_a,IdealGear01.flange_b);
  connect (SignalVoltage1.v,Limiter1.y);
  connect (Limiter1.u,Gain1.y);
  connect (Add1.u1,Gain2.y);
  connect (Gain1.u,Add1.y);
  connect (Gain2.u,u);  
   connect (flange_b,IdealGear01.flange_b); 
    end ServoMotor ;

model stepping
  ////Generation automatique ////
      //parameters
      parameter Real amplitude = 1.000000e+00;
      parameter Real starttime = 0.000000e+00;
      parameter Real mode = 2.000000e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput Tcommut;
      Modelica.Blocks.Interfaces.RealInput sens;
      //les variables disponibles sont Tcommut.signal signal sans unité, 
      //output variables
      Modelica.Blocks.Interfaces.RealOutput yA;
      //les variables disponibles sont yA.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput yB;
      //les variables disponibles sont yB.signal signal sans unité, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;

Real T0(final start = starttime) "Start time of current period";
Real nb_period(start = if mode>=2 then 8 else 4);
    Real sortie1;
    Real sortie2;

equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire

        der( T0 ) = 0;
der(nb_period)=0;
        when time > T0 + nb_period*Tcommut.signal then
          reinit( T0, time );
        end when;

        sortie1 = if mode >=0 and mode <1 then 
			if time >= T0 and time < T0+Tcommut.signal then amplitude elseif time >= T0+2*Tcommut.signal and time <T0+3*Tcommut.signal then -1*amplitude else 0 
		else if mode >=1 and mode <2 then
			if time >= T0 and time < T0+2*Tcommut.signal then amplitude else -1*amplitude
		else if mode >=2 and mode <3 then
        		if time >= T0 and time < T0+3*Tcommut.signal then amplitude elseif time >= T0+4*Tcommut.signal and time < T0+7*Tcommut.signal then -1*amplitude else 0
		else 0;
	sortie2 = if mode >=0 and mode <1 then 
			if time >= T0+Tcommut.signal and time < T0+2*Tcommut.signal then amplitude elseif time >= T0+3*Tcommut.signal and time <T0+4*Tcommut.signal then -1*amplitude else 0
		else if mode >=1 and mode <2 then
			if time >= T0+Tcommut.signal and time < T0+3*Tcommut.signal then amplitude else -1*amplitude
		else if mode >=2 and mode <3 then
    			if (time >= T0+Tcommut.signal and time < T0+2*Tcommut.signal) then 0 elseif time >= T0+2*Tcommut.signal and time < T0+5*Tcommut.signal then amplitude elseif time >= T0+5*Tcommut.signal and time < T0+6*Tcommut.signal then 0 else -1*amplitude
		else 0;
        
        yA.signal=if sens.signal >0 then sortie1 else sortie2;
        yB.signal=if sens.signal >0 then sortie2 else sortie1;
    
end stepping;

model fullstep
      //parameters
      parameter Real amplitude = 1.000000e+00;
      parameter Real starttime = 0.000000e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput Tcommut;
      //les variables disponibles sont Tcommut.signal signal sans unité, 
      //output variables
      Modelica.Blocks.Interfaces.RealOutput yA;
      //les variables disponibles sont yA.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput yB;
      //les variables disponibles sont yB.signal signal sans unité, 

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Real T0(final start = starttime) "Start time of current period";
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire

        der( T0 ) = 0;
        when time > T0 + 4*Tcommut.signal then
          reinit( T0, time );
        end when;

        yA.signal = if time >= T0 and time < T0+Tcommut.signal then amplitude elseif time >= T0+2*Tcommut.signal and time <T0+3*Tcommut.signal then -1*amplitude else 0;
	yB.signal = if time >= T0+Tcommut.signal and time < T0+2*Tcommut.signal then amplitude elseif time >= T0+3*Tcommut.signal and time <T0+4*Tcommut.signal then -1*amplitude else 0;

end fullstep;    
    
    
    model fullstepmax

      //parameters
      parameter Real amplitude = 1.000000e+00;
      parameter Real starttime = 0.000000e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput Tcommut;
      //les variables disponibles sont Tcommut.signal signal sans unité, 
      //output variables
      Modelica.Blocks.Interfaces.RealOutput yA;
      //les variables disponibles sont yA.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput yB;
      //les variables disponibles sont yB.signal signal sans unité, 

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Real T0(final start = starttime) "Start time of current period";
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire

        der( T0 ) = 0;
        when time > T0 + 4*Tcommut.signal then
          reinit( T0, time );
        end when;
        
        yA.signal = if time >= T0 and time < T0+2*Tcommut.signal then amplitude else -1*amplitude ;
	yB.signal = if time >= T0+Tcommut.signal and time < T0+3*Tcommut.signal then amplitude else -1*amplitude;

 
end fullstepmax;    
    
    
    model halfstep
  ////Generation automatique ////
      //parameters
      parameter Real amplitude = 1.000000e+00;
      parameter Real starttime = 0.000000e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput Tcommut;
      //les variables disponibles sont Tcommut.signal signal sans unité, 
      //output variables
      Modelica.Blocks.Interfaces.RealOutput yA;
      //les variables disponibles sont yA.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealOutput yB;
      //les variables disponibles sont yB.signal signal sans unité, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Real T0(final start = starttime) "Start time of current period";
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
         der( T0 ) = 0;
        when time > T0 + 8*Tcommut.signal then
          reinit( T0, time );
        end when;
        
        yA.signal = if time >= T0 and time < T0+3*Tcommut.signal then amplitude elseif time >= T0+4*Tcommut.signal and time < T0+7*Tcommut.signal then -1*amplitude else 0;
        yB.signal = if (time >= T0+Tcommut.signal and time < T0+2*Tcommut.signal) then 0 elseif time >= T0+2*Tcommut.signal and time < T0+5*Tcommut.signal then amplitude elseif time >= T0+5*Tcommut.signal and time < T0+6*Tcommut.signal then 0 else -1*amplitude;

end halfstep;
    
   
    
    model SynchronousMotordq
      parameter Real R = 1; //resistance statorique
      parameter Real L = 0.001; //inductance statorique
      parameter Real M = 0.175; //mutuelle inductance 
      parameter Real p = 2; //nombre de paires de poles
      parameter Real J=0.1; //moment d'inertie du rotor kg.m2
      parameter Real pi = 3.14159265359;
      //ports d'entree et de sortie
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Modelica.Blocks.Interfaces.RealInput phif; //pour le flux rotorique
      Pin pa,pb,pc; //pins positifs pour chaque phase a,b,c
      
      //variables internes
      Real va,vb,vc,vd,vq ; //differences de potentiels
      Real id,iq; //courants
      Real theta,thetae; // angle rotorique
      Real w; //vitesse angulaire
      Real a;
      Real Lc=L-M;
      Real phid,phiq;
    equation
      va=pa.v;
      vb=pb.v;
      vc=pc.v;
      theta=flange_b.phi;
      thetae=theta*p;

      iq=sqrt(2/3)*(-sin(thetae)*pa.i-sin(thetae-2*pi/3)*pb.i-pc.i*sin(thetae-4*pi/3));
      id=sqrt(2/3)*(cos(thetae)*pa.i+cos(thetae-2*pi/3)*pb.i-pc.i*cos(thetae-4*pi/3));

      
      va=sqrt(2/3)*(cos(thetae)*vd-sin(thetae)*vq);
      vb=sqrt(2/3)*(cos(thetae-2*pi/3)*vd-sin(thetae-2*pi/3)*vq);
      vc=sqrt(2/3)*(cos(thetae-4*pi/3)*vd-sin(thetae-4*pi/3)*vq);
      w=der(theta);
      vd=R*id+der(phid)-phiq*w;
      vq=R*iq+der(phiq)+phid*w;
      phid=Lc*id+sqrt(3/2)*phif.signal;
      phiq=Lc*iq;

      a=der(w);

      J*a=-p*sqrt(3/2)*phif.signal*iq+flange_b.tau;
      //J*a=flange_b.tau;
    end SynchronousMotordq;    

    model SynchronousMotor
      parameter Real R = 1; //resistance statorique
      parameter Real L = 0.001; //inductance statorique
      parameter Real M = 0.175; //mutuelle inductance 
      parameter Real p = 2; //nombre de paires de poles
      parameter Real J=0.1; //moment d'inertie du rotor kg.m2
      parameter Real pi = 3.14159265359;
      //ports d'entree et de sortie
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Modelica.Blocks.Interfaces.RealInput phif; //pour le flux rotorique
      Pin pa,pb,pc; //pins positifs pour chaque phase a,b,c
      
      //variables internes
      Real va,vb,vc ; //differences de potentiels
      Real phia,phib,phic; //flux
      Real theta,thetae; // angle rotorique
      Real w; //vitesse angulaire
      Real a;
      Real neutre;
    equation
      va=pa.v-neutre;
      vb=pb.v-neutre;
      vc=pc.v-neutre;
      //0 = pa.i + n.i;
      //n.i+pa.i=0;
      //n.i+pb.i=0;
      //n.i+pc.i=0;
      //va=R*pa.i;
      //vb=R*pb.i;
      //vc=R*pc.i;
      //phia=0;
      //phib=0;
      //phic=0;
      va=R*pa.i+der(phia);
      vb=R*pb.i+der(phib);
      vc=R*pc.i+der(phic);
      phia=L*pa.i+M*(pb.i+pc.i)+phif.signal*cos(thetae);
      phib=L*pb.i+M*(pa.i+pc.i)+phif.signal*cos(thetae-2*pi/3.);
      phic=L*pc.i+M*(pa.i+pb.i)+phif.signal*cos(thetae-4*pi/3.);
//      phia=L*pa.i+M*(pb.i+pc.i)+0.175*cos(theta);
//      phib=L*pb.i+M*(pa.i+pc.i)+0.175*cos(theta-2*pi/3.);
//      phic=L*pc.i+M*(pa.i+pb.i)+0.175*cos(theta-4*pi/3.);  
        theta=flange_b.phi;    
      thetae=theta*p;
      w=der(theta);
      a=der(w);
      //J*a=phif.signal*p*(pa.i+pb.i+pc.i)-flange_b.tau;
      //J*a=-0.175*p*(cos(theta)*pa.i+cos(theta-2*pi/3.)*pb.i+cos(theta-4*pi/3.)*pc.i)+flange_b.tau;
      J*a=p*phif.signal*(sin(thetae)*pa.i+sin(thetae-2*pi/3.)*pb.i+sin(thetae-4*pi/3.)*pc.i)+flange_b.tau;
      pa.i+pb.i+pc.i=0;
      //J*a=flange_b.tau;
    end SynchronousMotor;   
 
 
 model IdealInverter
  ////Generation automatique ////
      //parameters
      parameter Real dephini = 0.000000e+00;
      //input variables
      Modelica.Blocks.Interfaces.RealInput ampl;
      //les variables disponibles sont ampl.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealInput thetae;
      //les variables disponibles sont thetae.signal signal sans unité, 
      //output variables
      Modelica.Electrical.Analog.Interfaces.NegativePin n1;
      //les variables disponibles sont n1.v potentiel en V, n1.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin n2;
      //les variables disponibles sont n2.v potentiel en V, n2.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin n3;
      //les variables disponibles sont n3.v potentiel en V, n3.i courant en A, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;

equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
//n1.v=-ampl.signal*cos(thetae.signal-dephini);
//n2.v=-ampl.signal*cos(thetae.signal-2*3.1416/3-dephini);
//n3.v=-ampl.signal*cos(thetae.signal+2*3.1416/3-dephini);   
n1.v=ampl.signal*cos(thetae.signal-3.1416/2-dephini);
n2.v=ampl.signal*cos(thetae.signal-2*3.1416/3-3.1416/2-dephini);
n3.v=ampl.signal*cos(thetae.signal+2*3.1416/3-3.1416/2-dephini);   
end IdealInverter;
 
     model WoundedRotorSynchronousMotor
      parameter Real R = 1; //resistance statorique
      parameter Real L = 0.001; //inductance statorique
      parameter Real M = -0.0005; //mutuelle inductance 
      parameter Real p = 2; //nombre de paires de poles
      parameter Real J=0.000001; //moment d'inertie du rotor kg.m2
      parameter Real fv=0.0001; //moment d'inertie du rotor kg.m2
      parameter Real pi = 3.14159265359;
      //ports d'entree et de sortie
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Modelica.Blocks.Interfaces.RealInput phif; //pour le flux rotorique
      Pin pa,pb,pc; //pins positifs pour chaque phase a,b,c
      
      //variables internes
      Real va,vb,vc ; //differences de potentiels
      Real phia,phib,phic; //flux
      Real theta,thetae; // angle reel, angle electrique
      Real w; //vitesse angulaire
      Real a; //acc angulaire
      Real neutre;
    equation
      va=pa.v-neutre;
      vb=pb.v-neutre;
      vc=pc.v-neutre;

      va=R*pa.i+der(phia);
      vb=R*pb.i+der(phib);
      vc=R*pc.i+der(phic);
      phia=L*pa.i+M*(pb.i+pc.i)+phif.signal*cos(thetae);
      phib=L*pb.i+M*(pa.i+pc.i)+phif.signal*cos(thetae-2*pi/3.);
      phic=L*pc.i+M*(pa.i+pb.i)+phif.signal*cos(thetae-4*pi/3.);
  
      theta=flange_b.phi;    
      thetae=theta*p;
      w=der(theta);
      a=der(w);

      J*a=p*phif.signal*(sin(thetae)*pa.i+sin(thetae-2*pi/3.)*pb.i+sin(thetae-4*pi/3.)*pc.i)+flange_b.tau-fv*w;
      pa.i+pb.i+pc.i=0;

    end WoundedRotorSynchronousMotor;   
  
     model PermanentMagnetSynchronousMotor
      parameter Real R = 1; //resistance statorique
      parameter Real L = 0.001; //inductance statorique
      parameter Real M = -0.0005; //mutuelle inductance 
      parameter Real p = 2; //nombre de paires de poles
      parameter Real J=0.000001; //moment d'inertie du rotor kg.m2
      parameter Real pi = 3.14159265359;
      parameter Real phif = 0.1; //flux permanent
      parameter Real fv = 0.001;
      //ports d'entree et de sortie
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Pin pa,pb,pc; //pins positifs pour chaque phase a,b,c
      
      //variables internes
      Real va,vb,vc ; //differences de potentiels
      Real phia,phib,phic; //flux
      Real theta,thetae; // angle rotorique
      Real w; //vitesse angulaire
      Real a;
      Real neutre;
    equation
      va=pa.v-neutre;
      vb=pb.v-neutre;
      vc=pc.v-neutre;

      va=R*pa.i+der(phia);
      vb=R*pb.i+der(phib);
      vc=R*pc.i+der(phic);
      phia=L*pa.i+M*(pb.i+pc.i)+phif*cos(thetae);
      phib=L*pb.i+M*(pa.i+pc.i)+phif*cos(thetae-2*pi/3.);
      phic=L*pc.i+M*(pa.i+pb.i)+phif*cos(thetae-4*pi/3.);

      theta=flange_b.phi;    
      thetae=theta*p;
      w=der(theta);
      a=der(w);

      J*a=p*phif*(sin(thetae)*pa.i+sin(thetae-2*pi/3.)*pb.i+sin(thetae-4*pi/3.)*pc.i)-fv*w+flange_b.tau;
      pa.i+pb.i+pc.i=0;

    end PermanentMagnetSynchronousMotor;   

   model EMF_SINUS "Electromotoric force (electric/mechanic transformer) sinus"
          parameter Real k = 1 "Transformation coefficient";
          parameter Real np = 2 "Nombre de paires de poles";
          parameter Real dphi = 0 "dephasage initial entre l angle rotor nul et le champ magnetique";
          Real v "Voltage drop between the two pins";
          Real i "Current flowing from positive to negative pin";
          Real phi "Angle of shaft flange with respect to support (= flange.phi - support.phi)";
          Real w "Angular velocity of flange relative to support";
          Modelica.Electrical.Analog.Interfaces.PositivePin p;
          Modelica.Electrical.Analog.Interfaces.NegativePin n;
          Modelica.Mechanics.Rotational.Interfaces.Flange_b flange;
        equation
          v = p.v - n.v;
          0 = p.i + n.i;
          i = p.i;
          phi = flange.phi;
          w = der(phi);
          k * w * sin(np*phi+dphi) = v;
          flange.tau =  -k*np * i*sin(np*phi+dphi);
        end EMF_SINUS;
        
    model stepper
      parameter Real R = 1; //resistance statorique
      parameter Real L = 0.001; //inductance statorique
      parameter Real k=0.02; //fcem
      parameter Real n = 200; //nombre de pas par tour
      parameter Real J=0.0001; //moment d'inertie du rotor kg.m2
      parameter Real pi = 3.14159265359;
      
      //ports d'entree et de sortie
      Modelica.Mechanics.Rotational.Interfaces.Flange_b flange_b;
      Pin pa,pb; //pins positifs pour chaque phase a,b,c
      
      //variables internes
      Real va,vb ; //differences de potentiels
      Real theta,thetae; // angle rotorique et micropas
      Real w; //vitesse angulaire
      Real a; //acc angulaire
    equation
      va=pa.v;
      vb=pb.v;

      theta=flange_b.phi;    
      thetae=theta*n/4;
      w=der(theta);
      a=der(w);
      va=R*pa.i+L*der(pa.i)+k*w*sin(thetae);
      vb=R*pb.i+L*der(pb.i)+k*w*sin(thetae+pi/2.);

      J*a=k*n/4*(sin(thetae)*pa.i+sin(thetae+pi/2.)*pb.i)+flange_b.tau;

end stepper;    

model StepperMotor2

        parameter Real R = 1.650000e+00;
      parameter Real L = 0.001;
      parameter Real k = 3.000000e-01;
      parameter Real J = 1.000000e-06;
      parameter Real fv = 1.000000e-01;
      parameter Real np = 5.000000e+01;
      parameter Real pi = 3.14159265359;
      //input variables
      Modelica.Electrical.Analog.Interfaces.PositivePin pA;
      //les variables disponibles sont pA.v potentiel en V, pA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nA;
      //les variables disponibles sont nA.v potentiel en V, nA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.PositivePin pB;
      //les variables disponibles sont pB.v potentiel en V, pB.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nB;
      //les variables disponibles sont nB.v potentiel en V, nB.i courant en A, 
      //output variables
      Modelica.Mechanics.Rotational.Interfaces.Flange_b rot;
      Modelica.Electrical.Analog.Basic.Resistor Resistor1(R=R);
      Modelica.Electrical.Analog.Basic.Resistor Resistor2(R=R);

      SIMM.EMF_SINUS emf1(k=k,np=np,dphi=0);
      SIMM.EMF_SINUS emf2(k=k,np=np,dphi=pi/2);
      Modelica.Mechanics.Rotational.Inertia rotor(J=J);
            Modelica.Electrical.Analog.Basic.Inductor Inductor1(L=L);
      Modelica.Electrical.Analog.Basic.Inductor Inductor2(L=L);
                  
    equation
      connect (pA,Resistor1.p);
      connect (Resistor1.n,Inductor1.p);
      connect (Inductor1.n,emf1.p);
      connect (emf1.n,nA);
      connect (pB,Resistor2.p);
      connect (Resistor2.n,Inductor2.p);
      connect (Inductor2.n,emf2.p);
      connect (emf2.n,nB);
      
      connect (emf1.flange,rotor.flange_a);
      connect (emf2.flange,rotor.flange_a);
      connect (rotor.flange_b,rot);
end StepperMotor2;


model IdealCurrentSourcesStepper
  ////Generation automatique ////
      //input variables
      Modelica.Blocks.Interfaces.RealInput uA;
      //les variables disponibles sont uA.signal signal sans unité, 
      Modelica.Blocks.Interfaces.RealInput uB;
      //les variables disponibles sont uB.signal signal sans unité, 
      //output variables
      Modelica.Electrical.Analog.Interfaces.PositivePin pA;
      //les variables disponibles sont pA.v potentiel en V, pA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nA;
      //les variables disponibles sont nA.v potentiel en V, nA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.PositivePin pB;
      //les variables disponibles sont pB.v potentiel en V, pB.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nB;
      //les variables disponibles sont nB.v potentiel en V, nB.i courant en A, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Modelica.Electrical.Analog.Sources.SignalCurrent Scur1;
Modelica.Electrical.Analog.Sources.SignalCurrent Scur2;
Modelica.Electrical.Analog.Basic.Ground gnd;

equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
        connect(uA,Scur1.i);
	connect(uB,Scur2.i);
	connect(Scur1.p,gnd.p);
	connect(Scur2.p,gnd.p);
	connect(Scur1.n,pA);
	connect(Scur2.n,pB);
	connect(gnd.p,nA);
connect(gnd.p,nB);

end IdealCurrentSourcesStepper;

model StepperMotor
  ////Generation automatique ////
      //parameters
      parameter Real R = 1.650000e+00;
      parameter Real L = 0.001;
      parameter Real k = 3.000000e-01;
      parameter Real J = 1.000000e-06;
      parameter Real fv = 1.000000e-01;
      parameter Real np = 5.000000e+01;
      parameter Real pi = 3.14159265359;
      //input variables
      Modelica.Electrical.Analog.Interfaces.PositivePin pA;
      //les variables disponibles sont pA.v potentiel en V, pA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nA;
      //les variables disponibles sont nA.v potentiel en V, nA.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.PositivePin pB;
      //les variables disponibles sont pB.v potentiel en V, pB.i courant en A, 
      Modelica.Electrical.Analog.Interfaces.NegativePin nB;
      //les variables disponibles sont nB.v potentiel en V, nB.i courant en A, 
      //output variables
      Modelica.Mechanics.Rotational.Interfaces.Flange_b rot;
      //les variables disponibles sont rot.phi angle en rad, rot.tau couple en Nm, 
  ////Ne pas modifier avant cette ligne ////

 //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
Real vA;
Real iA;
Real vB;
Real iB;
Real phi;
Real w;
Real dphiA(start=0), dphiB(start=0);
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
             vA = pA.v - nA.v;
             //nA.v=nB.v;
          0 = pA.i + nA.i;
          iA = pA.i;
             vB = pB.v - nB.v;
          0 = pB.i + nB.i;
          iB = pB.i;
          der(dphiA)=0;
          der(dphiB)=0;      
      when L>0 then 
      dphiA=L*der(iA);
      dphiB=L*der(iB);
      end when;

          phi = rot.phi;
          w = der(phi);
          k * w * sin(np*phi) +R*iA +dphiA= vA;
	  k * w * sin(np*phi+3.1416/2) +R*iB +dphiB = vB;
          
          
          J*der(w)=-rot.tau  +k*np * iA*sin(np*phi)+ k*np * iB*sin(np*phi+3.1416/2)- fv*w;
end StepperMotor;

end SIMM;


package Hydraulic "Librairie hydraulique élémentaire"
  package Connectors
    package Interfaces
    
      connector PortA "Layout of port where oil flows into an element"
        Real p "pressure at port";
        flow Real q "flow rate through port";
      end PortA;
      
      connector PortB "Hydraulic port where the fluid leaves the component"
        Real p "pressure at port";
        flow Real q "flow rate through port";
      end PortB;
      
      partial model pOnePort "Décrit un composant qui a un port hydraulique en entrée"
        PortA portA ;
      end pOnePort;
      
      model Chamber "Fluide dans une chambre avec un piston et une pression (sans compressibilite du fluide)"
        extends SIMM.PartialCompliant;
        parameter Real PistonArea  "Aire du piston";
        parameter Real RodLength  "Longueur de la tige";
        Real v_rel "Relative velocity between flange flange_a and flange_b";
        Real stroke "stroke of chamber";
        Real Volume "Volume = stroke * piston_area";
        PortA port_A "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      equation
        stroke = s_rel - RodLength;
        Volume = stroke * PistonArea;
        port_A.q = -v_rel * PistonArea;
        flange_a.f = port_A.p * PistonArea;
        v_rel = der(s_rel);
      end Chamber;
      
      partial model TwoPortComponent "Superclass of circuits with two hydraulic ports"
        Real dp "pressure drop";
        Real q "flow rate through component";
        PortA portA "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
        PortB portB "Port B, were oil leaves the component (negative q, port_A.p > port_B.p means positive dp)" ;
      equation
        dp = portA.p - portB.p;
        q = portA.q;
        portA.q + portB.q = 0;
      end TwoPortComponent;
      
      partial model pTwoPortSystem "Superclass of circuits with two hydraulic ports"
        PortA port_A "Layout of port where oil flows into an element" ;
        PortB port_B "Hydraulic port where oil leaves the component" ;
       
      end pTwoPortSystem;
    end Interfaces;
    
    package Types
      type HydraulicCapacity = Real;
      type Conductance = Real;
    end Types;

    end Connectors;

 
  package Restrictions "Résistances hydrauliques"
    model Laminar "Resistance with laminar flow."
      extends Hydraulic.Connectors.Interfaces.TwoPortComponent;
      parameter Real G = 0.00000000000042 "conductance of laminar resistance";
    equation
      q = G * dp;
    end Laminar;
    
    model Turbulent "Resistance with turbulent flow."
      extends Hydraulic.Connectors.Interfaces.TwoPortComponent;
      parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
      parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
      parameter Real Pref = 0.000000001 "pressure ref for sign estimation";
    equation
      q = Qnom * sqrt(abs(dp) / DPnom) * tanh(dp / Pref);
    end Turbulent;
    
    model TurbulentControled "Controled resistance with turbulent flow."
      extends Hydraulic.Connectors.Interfaces.TwoPortComponent;
      parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
      parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
      parameter Real Imax = 1 "Maximal input";
    protected
      parameter Real Pref = 500000.0 "pressure ref for sign estimation and laminar approximation";
      parameter Real Iref = 0.000000001 "current ref for sign estimation";
    public
      Modelica.Blocks.Interfaces.RealInput i ;
    equation
      q = (Qnom * (tanh(i.signal / Iref) + 1)) / 2.0 * abs(i.signal / Imax) * sqrt(abs(dp) / DPnom) * tanh(dp / Pref);
    end TurbulentControled;
   
  end Restrictions;
  package Parts
    model RotComp "Perfect converter hydraulic-rotational"
      parameter Real Cyl = 1 "capacity volume (m^3/tr)";
      Real dp "pressure drop";
      Real q "débit dans le composant";
      Real w "angular velocity of pump shaft";
      Connectors.Interfaces.PortA portA "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB portB "Port B, were oil leaves the component (negative q, port_A.p > port_B.p means positive dp)" ;
      Modelica.Mechanics.Rotational.Interfaces.Flange_a flange ;
    equation
      dp = portA.p - portB.p;
      q = portA.q;
      portA.q + portB.q = 0;
      w = der(flange.phi);
      // Pompe parfaite
      q = (Cyl * w) / 2.0 / Modelica.Constants.pi;
      dp = (flange.tau * 2.0 * Modelica.Constants.pi) / Cyl;
    end RotComp;
    
    model ChamberLeft "Incompressible fluid in a chamber with piston and pressure"
      parameter Real PistonArea  "Piston area";
      Real v_rel "Relative velocity between flange flange_a and flange_b";
      Real length "lenght of chamber";
      Real Volume "Volume = stroke * piston_area";
      Connectors.Interfaces.PortA port_A "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Modelica.Mechanics.Translational.Interfaces.Flange_a flange_a ;
      Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b ;
    equation
      // Efforts
      flange_a.f = flange_b.f;
      flange_a.f = port_A.p * PistonArea;
      // Deplacement
      Volume = length * PistonArea;
      port_A.q = -v_rel * PistonArea;
      v_rel = der(length);
      // Positions
      flange_b.s = flange_a.s + length;
    end ChamberLeft;
    model ChamberRight "Incompressible fluid in a chamber with piston and pressure"
      parameter Real PistonArea  "Aire du piston";
      Real v_rel "Relative velocity between flange flange_a and flange_b";
      Real length "lenght of chamber";
      Real Volume "Volume = stroke * piston_area";
      Connectors.Interfaces.PortA port_A "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Modelica.Mechanics.Translational.Interfaces.Flange_a flange_a ;
      Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b ;
    equation
      // Efforts
      flange_a.f = flange_b.f;
      flange_a.f = -port_A.p * PistonArea;
      // Deplacement
      Volume = length * PistonArea;
      port_A.q = v_rel * PistonArea;
      v_rel = der(length);
      // Positions
      flange_b.s = flange_a.s + length;
    end ChamberRight;
    
    model VolEfficiency "Leakage corresponding to volumetric efficiency"
      parameter Real Nuv = 0.95 "Rendement volumetrique";
      parameter Real Pref = 0.001 "Puissance de référence";
      Real qleak "débit de fuite";
      Real DP "Différence de pression";
      Connectors.Interfaces.PortA portA "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB portB "Port B, were oil leaves the component (negative q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB portC ;
    equation
      portA.p = portB.p;
      DP = portA.p - portC.p;
      qleak + portC.q = 0;
      portA.q + portB.q + portC.q = 0;
      qleak = ((1 - Nuv) * portA.q * (tanh((DP * portA.q) / Pref) + 1)) / 2.0 + ((1 - Nuv) * portB.q * (tanh((DP * portB.q) / Pref) + 1)) / 2.0;
    end VolEfficiency;
    
    model HydraulicCapacity "Capacité hydraulique"
      parameter Real Ch = 0.00001 "Hydraulic capacity (m^3/Pa)";
      Connectors.Interfaces.PortA port_A "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
    equation
      port_A.q = Ch * der(port_A.p);
    end HydraulicCapacity;

end Parts;

  package Components
    package Sources "Contient les sources hydrauliques"
      model FlowVolumetric "convertit un signal en débit (m^3/s)"
        Connectors.Interfaces.PortB port_B "débit en m^3/s" ;
        Modelica.Blocks.Interfaces.RealInput signal "Signal d'entrée" ;
      equation
        port_B.q = -signal.signal;
      end FlowVolumetric;
      
      model Pressure "convertit un signal en pression (Pa) or Bar"
        parameter Real isPascal = 1;
        Connectors.Interfaces.PortB port_B "pression en Pa" ;
        Modelica.Blocks.Interfaces.RealInput signal "Signal d'entrée" ;
      equation
        port_B.p = if isPascal > 0.0 then signal.signal else signal.signal / 100000.0;
      end Pressure;
      
    end Sources;
    
    package Sensors "Mesure des grandeurs hydraulique"
        
        model Pressure "Ideal pressure sensor"
            Connectors.Interfaces.PortA port_A ;
            Modelica.Blocks.Interfaces.RealOutput p;
        equation
            p.signal = port_A.p;
            0= port_A.q;
        end Pressure;
        
        model FlowVolumetricSensor "Ideal sensor for volume flow rate"
          Connectors.Interfaces.PortA port_A ;
          Connectors.Interfaces.PortA port_B ;
          Modelica.Blocks.Interfaces.RealOutput q "débit mesuré";
        equation
          port_A.p = port_B.p;
          port_A.q = q.signal;
          port_B.q =  -q.signal;
        end FlowVolumetricSensor;      
    end Sensors;
 
 model CheckValve
      //input variables
      Connectors.Interfaces.PortA pr;
      //les variables disponibles sont pr.p pression en Pa, pr.q débit en m3/s, 
      //output variables
      Connectors.Interfaces.PortB n;
      //les variables disponibles sont n.p pression en Pa, n.q débit en m3/s, 
  ////Ne pas modifier avant cette ligne ////

  //    Vous pouvez definir après ce commentaire d'autres variables internes ou parametres
 //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;
        
          Real s (start = 0) "Auxiliary variable: if on then flow, if opened then pressure";
equation
      // Renseigner ici votre fonction
      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.
      // Exemple : der(flange.phi) pour la dérivée de la position angulaire
          pr.p-n.p = s * (if s < 0 then 1 else 1e-5);
          n.q = s * (if s < 0 then 1e-5 else 1) ;
         pr.q+n.q=0;
end CheckValve;
 
     model Cylinder "double acting cylinders"
      parameter Real Stroke = 0.05 "Course totale";
      parameter Real MobileMass  "Masse mobile (piston + tiges)";
      parameter Real PistonAreaLeft  "Effective area of left side of piston";
      parameter Real PistonAreaRight  "Effective area of right side of piston";
      Coselica.Mechanics.Translational.Components.Mass Piston(L = 0, m = MobileMass) ;
      Parts.ChamberLeft chamberPartialA(PistonArea = PistonAreaLeft) ;
      Parts.ChamberRight chamberPartialB(PistonArea = PistonAreaRight) ;
      Modelica.Mechanics.Translational.Interfaces.Flange_a flange_a "Left flange" ;
      Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b "Right flange" ;
      Modelica.Mechanics.Translational.Fixed FixedLeft(s0 = -Stroke / 2) ;
      Modelica.Mechanics.Translational.Fixed FixedRight(s0 = Stroke / 2) ;
      Connectors.Interfaces.PortA port_A1 "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)";
      Connectors.Interfaces.PortB port_A2 "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
    equation
      connect(chamberPartialA.flange_b,Piston.flange_a) ;
      connect(chamberPartialA.port_A,port_A1) ;
      connect(chamberPartialB.port_A,port_A2) ;
      connect(flange_a,flange_a) ;
      connect(FixedLeft.flange_b,chamberPartialA.flange_a) ;
      connect(FixedRight.flange_b,chamberPartialB.flange_a) ;
      connect(chamberPartialB.flange_b,Piston.flange_b) ;
      connect(Piston.flange_a,flange_a) ;
      connect(Piston.flange_b,flange_b) ;
    end Cylinder;
    
    model SimpleCylinder
      parameter Real k =1 "Raideur ressort de rappel" ;
      parameter Real d =1 "Amortissement ressort de rappel" ;
      parameter Real PistonAreaLeft = 0.001 "Effective area of left side of piston" ;
      parameter Real MobileMass = 100 "Masse mobile (piston + tige)" ;
      parameter Real Stroke = 0.05 "Course totale" ;
      Modelica.Mechanics.Translational.Fixed fixed1(s0 = -Stroke / 2) ;
      Modelica.Mechanics.Translational.Fixed fixed2(s0 = Stroke / 2) ;
      Coselica.Mechanics.Translational.Components.Mass Piston(L = 0, m = MobileMass) ;
      Hydraulic.Parts.ChamberLeft chamberleft1(PistonArea = PistonAreaLeft) ;
      Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b ;
      Modelica.Mechanics.Translational.SpringDamper springdamper1(c = k, d = d);
      Hydraulic.Connectors.Interfaces.PortA porta1 ;
    equation
      connect(springdamper1.flange_b,fixed1.flange_b) ;
      connect(Piston.flange_b,springdamper1.flange_a) ;
      connect(Piston.flange_b,flange_b) ;
      connect(fixed2.flange_b,chamberleft1.flange_a) ;
      connect(chamberleft1.flange_b,Piston.flange_a) ;
      connect(chamberleft1.port_A,porta1) ;
    end SimpleCylinder;
    
    
    model Reservoir "reservoir avec précharge mais sans resistance"
      extends Hydraulic.Connectors.Interfaces.pOnePort;
      parameter Real ppreload = 0.0 "preload pressure";
    equation
      portA.p = ppreload;
    end Reservoir;
    model Valve33 "Valve with 3 ports and 3 positions"
      parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
      parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
      parameter Real Imax = 1 "Maximal input";
      Connectors.Interfaces.PortA port_P "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB port_T "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Restrictions.TurbulentControled turbulentControled(DPnom = DPnom, Qnom = Qnom, Imax = Imax);
      Connectors.Interfaces.PortA portA ;
      Restrictions.TurbulentControled turbulentControled1(DPnom = DPnom, Qnom = Qnom, Imax = Imax);
      Modelica.Blocks.Interfaces.RealInput i1 ;
      Modelica.Blocks.Math.Gain gain(k = -1) ;
    equation
      connect(turbulentControled.portB,port_P) ;
      connect(turbulentControled.portA,portA) ;
      connect(portA,portA) ;
      connect(port_T,turbulentControled1.portB) ;
      connect(turbulentControled1.portA,portA) ;
      connect(turbulentControled.i,i1) ;
      connect(gain.y,turbulentControled1.i) ;
      connect(gain.u,i1) ;
    end Valve33;
    
model Valve43 "Valve with 4 ports and 3 positions"
      parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
      parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
      parameter Real Imax = 1 "Maximal input";
      Connectors.Interfaces.PortA port_P "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB port_T "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Restrictions.TurbulentControled turbulentControled(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
      Connectors.Interfaces.PortA portA ;
      Restrictions.TurbulentControled turbulentControled1(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
      Modelica.Blocks.Interfaces.RealInput i1 ;
      Modelica.Blocks.Math.Gain gain(k = -1) ;
      Restrictions.TurbulentControled turbulentControled2(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
      Restrictions.TurbulentControled turbulentControled3(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
      Connectors.Interfaces.PortB portB ;
    equation
      connect(gain.u,i1);
      connect(i1,turbulentControled.i) ;
      connect(gain.y,turbulentControled1.i) ;
      connect(turbulentControled3.i,i1) ;
      connect(turbulentControled2.i,gain.y) ;
      connect(port_P,turbulentControled.portA) ;
      connect(turbulentControled.portB,turbulentControled1.portA) ;
      connect(turbulentControled1.portB,turbulentControled2.portA) ;
      connect(turbulentControled3.portA,turbulentControled2.portB) ;
      connect(turbulentControled3.portB,turbulentControled.portA) ;
      connect(portA,turbulentControled.portB) ;
      connect(port_T,turbulentControled1.portB) ;
      connect(portB,turbulentControled2.portB) ;
    end Valve43;
    
    model Valve22 "Valve with 2 ports and 2 positions"
      parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
      parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
      parameter Real Imax = 1 "Maximal input";
      Modelica.Blocks.Math.Gain gain(k = -1) ;
      Modelica.Blocks.Interfaces.RealInput i1 ;
      Restrictions.TurbulentControled turbulentControled(DPnom = DPnom, Qnom = Qnom, Imax = Imax) ;
      Connectors.Interfaces.PortA portA ;
      Connectors.Interfaces.PortB portB ;
    equation
      connect(gain.u,i1);
      connect(turbulentControled.i,i1) ;
      connect(turbulentControled.portB,portB) ;
      connect(turbulentControled.portA,portA) ;
    end Valve22;

model Valve53 "Valve with 5 ports and 3 positions"
  parameter Real DPnom = 7000000.0 "Nominal difference of pressure (en Pa)";
  parameter Real Qnom = 1 "Nominal flow rate (en m^3/s)";
  parameter Real Imax = 1 "Maximal input";
  Restrictions.TurbulentControled turbulentControled(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
  Restrictions.TurbulentControled turbulentControled1(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
  Modelica.Blocks.Interfaces.RealInput i1 ;
  Modelica.Blocks.Math.Gain gain(k = -1) ;
  Restrictions.TurbulentControled turbulentControled2(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
  Restrictions.TurbulentControled turbulentControled3(Qnom = Qnom, Imax = Imax, DPnom = DPnom / 2) ;
  Connectors.Interfaces.PortA portA ;
  Connectors.Interfaces.PortB portC ;
  Connectors.Interfaces.PortA port_P "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
  Connectors.Interfaces.PortB port_T "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
  Hydraulic.Connectors.Interfaces.PortB portB ;
equation
  connect(gain.u,i1) ;
  connect(i1,turbulentControled.i) ;
  connect(gain.y,turbulentControled1.i) ;
  connect(turbulentControled3.i,i1) ;
  connect(turbulentControled2.i,gain.y) ;
  connect(port_P,turbulentControled.portA) ;
  connect(turbulentControled.portA,turbulentControled1.portA) ;
  connect(turbulentControled.portB,turbulentControled2.portB) ;
  connect(turbulentControled2.portA,turbulentControled3.portA) ;
  connect(portA,turbulentControled1.portB) ;
  connect(port_T,turbulentControled3.portA) ;
  connect(portB,turbulentControled2.portB) ;
  connect(portC,turbulentControled3.portB) ;
end Valve53;


    
    model PressureForceTranslator "Equals pressure and force for a given area (parameter)"
      parameter Real Area=1;
      Connectors.Interfaces.PortA port ;
      Modelica.Mechanics.Translational.Interfaces.Flange_a flange ;
    equation
      port.p = flange.f / Area;
    end PressureForceTranslator;
    model Pump "Convertisseur Hydraulique / Mecanique rotation parfait"
      parameter Real Cyl = 1 "Volume de la cylindrée (m^3/tour)";
      Real dp "pressure drop";
      Real q "débit dans le composant";
      Real w "angular velocity of pump shaft";
      Connectors.Interfaces.PortA portA "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB portB "Port B, were oil leaves the component (negative q, port_A.p > port_B.p means positive dp)" ;
      Modelica.Mechanics.Rotational.Interfaces.Flange_a flange ;
    equation
      dp = portA.p - portB.p;
      q = portA.q;
      portA.q + portB.q = 0;
      w = der(flange.phi);
      // Pompe parfaite
      q = (Cyl * w) / 2 / Coselica.Constants.pi;
      dp = (flange.tau * 2 * Coselica.Constants.pi) / Cyl;
    end Pump;

    model PumpCylinderVariable "Convertisseur Hydraulique / Mecanique rotation avec cylindree variable"
      Modelica.Blocks.Interfaces.RealInput u ;
      Real dp "pressure drop";
      Real q "débit dans le composant";
      Real w "angular velocity of pump shaft";
      Connectors.Interfaces.PortA portA "Port A, were oil flows into the component (positive q, port_A.p > port_B.p means positive dp)" ;
      Connectors.Interfaces.PortB portB "Port B, were oil leaves the component (negative q, port_A.p > port_B.p means positive dp)" ;
      Modelica.Mechanics.Rotational.Interfaces.Flange_a flange ;

    equation

      dp = portA.p - portB.p;
      q = portA.q;
      portA.q + portB.q = 0;
      w = der(flange.phi);
      // Pompe parfaite
      q = (u.signal * w) / 2 / Coselica.Constants.pi;
      dp = (flange.tau * 2 * Coselica.Constants.pi) / u.signal;
    end PumpCylinderVariable;
    
  end Components;
end Hydraulic;










package Aeraulic "Librairie Aeraulique élémentaire"
    package Interfaces
    
      connector PortA "Entree dans un element"
        Real p "pression";
        Real r "poids d eau";
        Real T "Temperature";
        flow Real qm "debit massique air sec";
      end PortA;
      
      connector PortB "Sortie d un element"
        Real p "pression";
        Real r "poids d eau";
        Real T "Temperature";
        flow Real qm "debit massique air sec";
      end PortB;
      
    end Interfaces;

  package Sources

       model SourceAir "Source d air a potentiels constants"
        parameter Real T  "temperature renseignee en degre";
        parameter Real H  "humidite relative en %";
        parameter Real P "pression en Pa";
        Real pvs "pression de vapeur saturante";
        Real pv "pression de vapeur";
        Interfaces.PortB sortie "Connecteur de sortie " ;
      equation
        
        if (T>0) then
            pvs=10^(2.7877+7.625*T/(241.6+T));
        else
            pvs=10^(2.7877+9.725*T/(272.2+T))   ;
        end if;
        pv=H*pvs/100; 
        sortie.r=0.622*pv/(P-pv);
        sortie.T=T;
        sortie.p=P;
      end SourceAir;

       model SortieAir "Sortie d air a potentiels constants"
        parameter Real T  "temperature renseignee en degre";
        parameter Real H  "humidite relative en %";
        parameter Real P "pression en Pa";
        Interfaces.PortA entree "Connecteur d entree " ;
      equation
        entree.p=P;
      end SortieAir;

   end Sources;

    package Sensors "Mesure des grandeurs hydraulique"
        model Pressure "Capteur de pression"
            Interfaces.PortA entree ;
            Modelica.Blocks.Interfaces.RealOutput p;
        equation
            p.signal = entree.p;
            0= entree.qm;
        end Pressure;

        model Temperature "capteur de temperature"
            Interfaces.PortA entree ;
            Modelica.Blocks.Interfaces.RealOutput T;
        equation
            T.signal = entree.T;
            0= entree.qm;
        end Temperature;

        model Hygrometry "capteur d humidite relative"
            Interfaces.PortA entree ;
            Modelica.Blocks.Interfaces.RealOutput H;
            Real pv;
            Real pvs;
        equation
            pv=entree.p*entree.r/(0.622+entree.r);
            if (entree.T>0) then
                pvs=10^(2.7877+7.625*entree.T/(241.6+entree.T));
            else
                pvs=10^(2.7877+9.725*entree.T/(272.2+entree.T))   ;
            end if;
           H.signal = pv/pvs*100;
            0= entree.qm;
            
        end Hygrometry;
        
        model FlowMassicSensor "capteur de debit massique"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealOutput qm "débit mesuré";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm=0;
          qm.signal= entree.qm;
        end FlowMassicSensor;
              
    end Sensors;
    
    package Components "Composants aerauliques"
      model BatterieChaude "Batterie Chaude"
        parameter Real Z  "Coefficient de perte de charge Aéraulique Pa.h²/m6";
        parameter Real ETA  "Rendement de la batterie %";
        Real he "enthalpie d''entrée";
        Real hs "enthalpie de sortie";
        Real vs "volume spécifique";
        Modelica.Blocks.Interfaces.RealInput Pt;
        Interfaces.PortB sortie "Connecteur de sortie ";
        Interfaces.PortA entree "Connecteur d''entrée" ;
      equation
        sortie.r = entree.r "Conservation de la masse d''eau";
        sortie.qm + entree.qm = 0 "Conservation de la masse d''air sec";
        he = 1.006*entree.T+entree.r*(2501+1.83*entree.T);
        hs = he + Pt.signal / entree.qm;
        vs = 461.24*(0.622+entree.r)*(273.15+entree.T)/entree.p "calcul du volume spécifique au caractéristiques de l''air d''entrée";
        sortie.T = (hs - sortie.r*2501)/(1.006+entree.r*1.83); 
        //sortie.p = entree.p - Z*(entree.qm*vs)^2;
        entree.qm=sqrt((-sortie.p + entree.p)/Z)/vs;
      end BatterieChaude;
        
        model BatterieFroide "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealInput Pt "Puissance thermique injectee";
            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm + Pt.signal=0;
        end BatterieFroide;

        model Humidificateur "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealInput Pt "Puissance thermique injectee";
            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm + Pt.signal=0;
        end Humidificateur;
        
        
        model ConduiteAeraulic "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealInput Pt "Puissance thermique injectee";
            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm + Pt.signal=0;
        end ConduiteAeraulic;

        model Ventilateur "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealInput Pt "Puissance thermique injectee";
            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm + Pt.signal=0;
        end Ventilateur;

        model Registre "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie ;
          Modelica.Blocks.Interfaces.RealInput Pt "Puissance thermique injectee";
            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree.p = sortie.p;
          entree.T = sortie.T;
          entree.r = sortie.r;
          sortie.qm + entree.qm + Pt.signal=0;
        end Registre;

        model Echangeur "Batterie froide"
          Interfaces.PortA entree1 ;
          Interfaces.PortB sortie1 ;
          Interfaces.PortA entree2 ;
          Interfaces.PortB sortie2 ;

            parameter Real T  "temperature renseignee en degre";
            parameter Real H  "humidite relative en %";
            parameter Real P "pression en Pa";
        equation
          entree1.p = sortie1.p;
          entree1.T = sortie1.T;
          entree1.r = sortie1.r;
          entree2.p = sortie2.p;
          entree2.T = sortie2.T;
          entree2.r = sortie2.r;
          sortie1.qm + entree1.qm =0;
          sortie2.qm + entree2.qm =0;
        end Echangeur;

        model CaissonMelange "Batterie froide"
          Interfaces.PortA entree1 ;
          Interfaces.PortA entree2 ;
          Interfaces.PortB sortie ;

          parameter Real T  "temperature renseignee en degre";
          parameter Real H  "humidite relative en %";
          parameter Real P "pression en Pa";
        equation
          entree1.p = sortie.p;
          entree1.T = sortie.T;
          entree1.r = sortie.r;
          entree2.p = sortie.p;
          entree2.T = sortie.T;
          entree2.r = sortie.r;
          sortie.qm + entree1.qm + entree2.qm =0;

        end CaissonMelange;

        model DivergenceAeraulic "Batterie froide"
          Interfaces.PortA entree ;
          Interfaces.PortB sortie1 ;
          Interfaces.PortB sortie2 ;

          parameter Real T  "temperature renseignee en degre";
          parameter Real H  "humidite relative en %";
          parameter Real P "pression en Pa";
        equation
          entree.p = sortie1.p;
          entree.T = sortie1.T;
          entree.r = sortie1.r;
          entree.p = sortie2.p;
          entree.T = sortie2.T;
          entree.r = sortie2.r;
          sortie1.qm + sortie2.qm + entree.qm =0;
        end DivergenceAeraulic;


    end Components;
 
 
 
end Aeraulic;

