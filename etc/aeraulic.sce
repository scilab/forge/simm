// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("aeraulic.sce")+"..";
    subdir=[ 'SIMM', 'Aeraulique']

    name_subpal= _("Sources")

    blocks=[
        "SourceAir"
        "SortieAir" 
           ];
    xpal = xcosPal(name_subpal);

    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal=_("Componants")

    blocks=[
    "BatterieChaude"  // 1 entree, 1 sortie, 1 entree signal
    "BatterieFroide" // 1 entree, 1 sortie, 1 entree signal
    "Humidificateur" // 1 entree, 1 sortie, 1 entree signal
    "ConduiteAeraulic" // 1 entree, 1 sortie, 1 entree signal
    "Ventilateur" // 1 entree, 1 sortie, 1 entree signale
    "Registre" // 1 entree, 1 sortie, 1 entree signale
    "Echangeur" // 2 entree , 2 sorties mixees
    "CaissonMelange" // 2 entree 1 sortie
    "DivergenceAeraulic" //1 entree 2 sortie
    ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Capteurs"
  
    blocks=[
        "PressureSensorAeraulic"
        "HygrometrySensor"
        "TempSensorAeraulic"
        "FlowMassicSensor"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

endfunction

runMe();
clear runMe;
