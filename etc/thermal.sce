// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("thermal.sce")+"..";
    subdir=[ 'SIMM', 'Thermique']

    name_subpal= _("Sources")
    cos_blocks = [
                    "MTH_FixedTemperature"
                      "MTHC_FixedTemperature"
                      "MTH_PrescribedTemperatur"
                      "MTHC_PrescribedTemperatu"
                      "MTH_FixedHeatFlow"
                      "MTH_PrescribedHeatFlow"
                  ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Basique"
    cos_blocks = [   "MTH_HeatCapacitor"
                    "MTH_ThermalConductor"
                    "MTH_Convection"
                    "MTH_BodyRadiation"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Conversion"
    cos_blocks = [ "MTHC_ToKelvin"
                      "MTHC_FromKelvin"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
     xcosPalAdd(xpal, subdir);

    name_subpal= "Mesure"
    cos_blocks = [
                    "MTH_TemperatureSensor"
                      "MTHC_TemperatureSensor"
                      "MTH_RelTemperatureSensor"
                      "MTH_HeatFlowSensor"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);
endfunction

runMe();
clear runMe;
