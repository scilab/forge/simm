// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
          loadXcosLibs();
    toolbox_dir = get_absolute_file_path("utilities.sce")+"..";
    //setlanguage('en');
    subdir=[ 'SIMM', _('Utilitaires')]

    name_subpal= "Visualisation"
    blocks = [
                  "ISCOPE"
                  "IREP_TEMP"
                  "SIMM_CONTROL"
//                  "ExtractPosBar"
//                  "AnimBar2D"
                  ];
    xcos_blocks=[
                "CSCOPE"
                "CMSCOPE"
                "CSCOPXY"
                "CLOCK_c"
    ];
    xpal = xcosPal(name_subpal);
    xpal=tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir,xpal);
    xpal=tbx_build_pal_from_xcos(toolbox_dir, name_subpal, xcos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Routage"
    //blocks from coselica
    cos_blocks = [
              "CBI_RealInput"
              "CBI_RealOutput"
              "CBR_DeMultiplex2"
              "CBR_DeMultiplexVector2"
              "CBR_Extractor"
              "CBR_Multiplex2"
              "CBR_MultiplexVector2"
              "CBR_Replicator"
                     ]
    xcos_blocks=[
                "MUX"
                 "DEMUX"
                 ]
    xpal = xcosPal(name_subpal);
    xpal=tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir,xpal);
    xpal=tbx_build_pal_from_xcos(toolbox_dir, name_subpal, xcos_blocks, subdir,xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Analyses"
    //blocks from coselica
    blocks = [
                  "IREP_TEMP"
                  "IPARAM_VAR"
                  "SIMMUserModel"
                     ]
   xcos_blocks=["ENDBLK"
                 ]
     xpal = xcosPal(name_subpal);
    xpal = tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xpal = tbx_build_pal_from_xcos(toolbox_dir, name_subpal, xcos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);
endfunction

runMe();
clear runMe;
