// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("mechanics_translational.sce")+"..";
    subdir=[ 'SIMM', 'Mecanique','Translation 1D']

    name_subpal= "Sources"
    cos_blocks = [
    "CMTS_ImposedKinematic"
                      "CMTS_Force0"
                      "CMTS_Force"
                      "CMTS_Force2"
//                      "CMTS_ConstantForce"
//                      "CMTS_ConstantSpeed"
//                      "CMTS_ForceStep"
//                      "CMTS_LinearSpeedDependen"
//                      "CMTS_QuadraticSpeedDepen"
                  ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Basique"
    cos_blocks = [
                    "MMT_Fixed"
                    "CMTC_Free"
                    "CMTC_Mass"
                    "CMTC_MassWithWeight"
                    "MMT_Rod"
                    "MMT_Spring"
                    "MMT_Damper"
                    "MMT_SpringDamper"
                    "CMTC_ElastoGap"
                    "CMT_MassWithFriction"

                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Mesure"
    cos_blocks = [
                      "CMTS_ForceSensor"
                      "CMTS_PowerSensor"
                      "CMTS_GenSensor"
                      "CMTS_GenRelSensor"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

endfunction

runMe();
clear runMe;
