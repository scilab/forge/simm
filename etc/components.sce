// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("components.sce")+"..";
    //setlanguage('en');
    subdir=[ 'SIMM', 'Composants']

//    name_subpal= _("Sources")
//    cos_blocks = [
//                  ]
//    xpal = xcosPal(name_subpal);
//    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir,xpal);
//    xcosPalAdd(xpal, subdir);

    name_subpal= "PréActionneurs"
        cos_blocks = [   
        "MEMC_Q1driver"
        "MEMC_Q2driver"
        "MEMC_Q4driver"
        
        
                 ]
    blocks = [
        "SteppingSequence"
        "IdealCurSourcesStepper"
        "IdealInverter"
        "OnduleurBLDC"
        "HallLogicBLDC"
             ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);
    //
    name_subpal= "Actionneurs"
    
    blocks = [
                  "ServoMotor"
                  "WoundedRotorSyncMotor"
                  "PermanentMagnetSyncMotor"
                  "StepperMotor"
                  ];
    cos_blocks = [   "MEMC_DCmotor"
                 ]
    xpal = xcosPal(name_subpal);
    xpal=tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir,xpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Adaptateurs"

    blocks = [
            "TrainEpiType1"
            "TrainEpiType2"
            "TrainEpiType3"
            "TrainEpiType4"
                  ];
                  
    cos_blocks = [
        "CMR_Brake"
        "CMR_Clutch"
        "CMR_OneWayClutch"
        "CMR_Freewheel"
        "MMR_IdealPlanetary"
        "CMRC_IdealDifferential"
        "MMR_IdealGearR2T"
        "CMT_Stop"
        "CMTC_Pulley"
        "CMTC_ActuatedPulley"
        "CMTC_Lever"
        "MMR_IdealGearGen"
        "MMR_IdealGearR2TGen"
                 ]

    xpal = xcosPal(name_subpal);
    xpal=tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir,xpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

//    name_subpal= _("Sensors")
//    cos_blocks = [
//                     ]
//
//    xpal = xcosPal(name_subpal);
//    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
//    xcosPalAdd(xpal, subdir);
endfunction

runMe();
clear runMe;
